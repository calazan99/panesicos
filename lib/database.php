<?php
class Database
{
    private static $connection;
    public static $error;
// se hace una coneccion a Mysql
    private static function connect()
    {
        // se comprueba la coneccion
        $server = "localhost";
        $database = "panes";
        $username = "root";
        $password = "";
        $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "set names utf8");
        $conexion = @mysql_connect($server, $username) or die("ERROR"); 
        $db = @mysql_select_db($database, $conexion) or die("fail to conect"); 
        self::$connection = null;
        try
        {
            self::$connection = new PDO("mysql:host=".$server."; dbname=".$database, $username, $password, $options);
            self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $exception)
        {
            die($exception->getMessage());
        }
    }

    private static function desconnect()
    {
        // se hace una variable de coneccion
        
        self::$connection = null;
    }
// librerias a ocupar
    public static function executeRow($query, $values)
    {
        self::connect();
        $statement = self::$connection->prepare($query);
        $state = $statement->execute($values);
        self::$error = $statement->errorInfo();
        self::desconnect();
        return $state;
    }

    public static function getRow($query, $values)
    {
        self::connect();
        $statement = self::$connection->prepare($query);
        $statement->execute($values);
        self::desconnect();
        return $statement->fetch(PDO::FETCH_BOTH);
    }

    public static function getRows($query, $values)
    {
        self::connect();
        $statement = self::$connection->prepare($query);
        $statement->execute($values);
        self::desconnect();
        return $statement->fetchAll(PDO::FETCH_BOTH);
    }

     public static function getCount($query, $values)
    {
        self::connect();
        $statement = self::$connection->prepare($query);
        $statement->execute($values);
        self::desconnect();
        return $statement->rowCount();
    }


 
    
}
?>