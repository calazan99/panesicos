<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <!--Import Google Icon Font-->
    <link href="css/icons.css" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>


     
   

     <body background="fondo2.jpg">
     
	  <?php
    include("inc/menu.php");
    ?>


    <div>
    <h5 class="center-align"><a href="#"><FONT FACE="Imprint MT Shadow" color="black" SIZE=12 >Ofertas de la semana</FONT></a>
         </div>

        <div>
        <h5 class="center-align"> <a href="#"><FONT FACE="Imprint MT Shadow" color="black" SIZE=5 >En esta area, 
    puedes  consultar las ofertas disponibles que estan en la semana, puedes ver los tipos de panes 
     que hay y poder agregar complementos, como seria la soda, agrandar el pan, etc.</FONT></a>.</h5>
        </div>
  
  <div class="carousel">
    <a class="carousel-item" href="#one!"><img src="img/carousel/img-1.jpg"></a>
    <a class="carousel-item" href="#two!"><img src="img/carousel/img-2.jpg"></a>
    <a class="carousel-item" href="#three!"><img src="img/carousel/img3.jpg"></a>
    <a class="carousel-item" href="#four!"><img src="img/carousel/img04.jpg"></a>
    <a class="carousel-item" href="#five!"><img src="img/carousel/img05.jpg"></a>
  </div>

 
<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>

 <!-- Sección de productos -->
	<div class='container' id='productos'>
		<h4 class='center-align'>NUESTROS PRODUCTOS</h4>
		<div class='row'>
		<?php
		require("../lib/database.php");
		$sql = "SELECT * FROM combos WHERE  estado_combo = 1";
		$data = Database::getRows($sql, null);
		if($data != null)
		{
			foreach ($data as $row) 
			{
				print("
					<div class='card hoverable col s12 m6 l4'>
						<div class='card-image waves-effect waves-block waves-light'>
							<img class='activator' src='data:image/*;base64,$row[imagen_combo]'>
						</div>
						<div class='card-content'>
							<span class='card-title activator grey-text text-darken-4'>$row[nombre_combo]<i class='material-icons right'>more_vert</i></span>
							<p><a href='#'><i class='material-icons left'>loupe</i>Seleccionar</a></p>
						</div>
						<div class='card-reveal'>
							<span class='card-title grey-text text-darken-4'>$row[nombre_combo]<i class='material-icons right'>close</i></span>
							<p>$row[descripcion_combo]</p>
							<p>Precio (US$) $row[precio_combo]</p>
						</div>
					</div>
				");
			}
		}
		else
		{
			print("<div class='card-panel yellow'><i class='material-icons left'>warning</i>No hay registros disponibles en este momento.</div>");
		}
		?>
		</div><!-- Fin de row -->
	</div><!-- Fin de container -->





    <?php
    require("inc/pie.php")
    ?>
</body>
</html>









