<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <!--Import Google Icon Font-->
    <link href="css/icons.css" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
     
    <?php
    include("inc/menu.php");
    ?>

     <body background="fondo2.jpg">

     <div>
    <h5 class="center-align">PREGUNTAS FRECUENTES</h5>   
  </div>
          <br>
              <p>En esta seccion puedes encontrar las preguntas que usualmente las personas hacen a cerca de como es el 
              funcionaminto de la pagina, ya que es un sistema que es nuevo y pocos saben manejar cosas asi.</p>
          <br>

 <!-- Aqui estan para poder ver el video sin coneccion a internet-->
 <div>
    <h5 class="center-align">  
    <video class="responsive-video" controls>
    <source src="comercial.mp4" type="video/mp4"> </video>
  </h5>
  </div>


    <table>
              <thead>
                <tr>
                <!-- Aqui estan las palabras para identificar el cuadro -->
                    <th>Numero</th>
                    <th>Pregunta</th>
                    <th>Respuestas</th>
                </tr>
              </thead>
              <!-- Aqui comienzan las preguntas y respuestas -->
               <tbody>
                  <tr>
                    <td>1</td>
                    <td>¿Qué inversión inicial se necesita para implementar una idea simlar?</td>
                    <td>La inversión inicial se sitúa entre los 130.000 € y los 186.000€ para una ubicación tradicional.</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>¿Quién fundo el sistema Panes Icos, en qué fecha y dónde?</td>
                    <td>Los fundadores fueron Rene Rivera Y Mario Estrada, unos estudiantes de El Salvador del Instituto Tecnico Ricaldone, a mediados de marzo del 2017.</td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>¿A que segmento del mercado va dirigido el sistema?</td>
                    <td>A adultos de entre 16 y 45 años.</td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td>¿Existe la posibilidad de poder contribuir con Panes Icos?</td>
                    <td>Claro que si, solo necesita un master en ingenieria.</td>
                  </tr>
                  <tr>
                    <td>5</td>
                    <td>¿Cuanto tomo desarrollar el sitio web?</td>
                    <td>Se tardo de un aproximado de 7 meses.</td>
                  </tr>
              </tbody>
            </table>



    <?php
    require("inc/pie.php")
    ?>
</body>
</html>






