<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <!--Import Google Icon Font-->
    <link href="css/icons.css" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
     
    <?php
    include("inc/menu.php");
    ?>

     <body background="fondo1.jpg">

<h5 class="center-align"> REGISTRARSE COMO NUEVO CLIENTE</h5>

     <?php
require("../lib/database.php");
require("../lib/validator.php");

if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$nombre_cliente = $_POST['nombre_cliente'];
  	$apellido_cliente = $_POST['apellido_cliente'];
    $direccion_cliente = $_POST['direccion_cliente'];
    $telefono_cliente = $_POST['telefono_cliente'];
    $estado_cliente = $_POST['estado_cliente'];
    $usuario_cliente = $_POST['usuario_cliente'];
    

    try 
    {
      	if($nombre_cliente != "" && $apellido_cliente != "")
        {
            if($direccion_cliente != "" && $telefono_cliente != "" )
            {
                    
                    if($usuario_cliente != "")
                    {
                        $contrasenia_cliente1 = $_POST['contrasenia_cliente1'];
                        $contrasenia_cliente2 = $_POST['contrasenia_cliente2'];
                        if($contrasenia_cliente1 != "" && $contrasenia_cliente2 != "")
                        {
                            if($contrasenia_cliente1 == $contrasenia_cliente2)
                            {
                                
                                $contrasenia_cliente = password_hash($contrasenia_cliente1, PASSWORD_DEFAULT);
                                $sql = "INSERT INTO clientes(nombre_cliente, apellido_cliente, direccion_cliente, telefono_cliente, estado_cliente, usuario_cliente, contrasenia_cliente) VALUES(?, ?, ?, ?, ?, ?, ?)";
                                $params = array($nombre_cliente, $apellido_cliente, $direccion_cliente, $telefono_cliente, $estado_cliente, $usuario_cliente, $contrasenia_cliente);
                                if(Database::executeRow($sql, $params))
                                {
                                    print("Bien");
                                }
                                else
                                {
                                    print_r(Database::$error);
                                }
                            }
                            else
                            {
                                throw new Exception("Las contraseñas no coinciden");
                            }
                        }
                        else
                        {
                            throw new Exception("Debe ingresar ambas contraseñas");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe ingresar un usuario");
                    }
        }
        else
        {
            throw new Exception("Debe ingresar direccion o telefono");
        }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }
    catch (Exception $error)
    {
        
    }
}
else
{
    $nombre_cliente = null;
    $apellido_cliente = null;
    $direccion_cliente = null;
    $telefono_cliente = null;
    $estado_cliente = null;
    $usuario_cliente = null;
}
?>

<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombre_cliente' type='text' name='nombre_cliente' class='validate' value='<?php print($nombre_cliente); ?>' required/>
          	<label for='nombre_cliente'>nombre_cliente</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellido_cliente' type='text' name='apellido_cliente' class='validate' value='<?php print($apellido_cliente); ?>' required/>
            <label for='apellido_cliente'>apellido_cliente</label>
        </div>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input id='direccion_cliente' type='text' name='direccion_cliente' class='validate' value='<?php print($direccion_cliente); ?>' required/>
            <label for='direccion_cliente'>direccion</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='telefono_cliente' type='text' name='telefono_cliente' class='validate' value='<?php print($telefono_cliente); ?>'/>
            <label for='telefono_cliente'>telefono</label>
        </div>
    </div>
    <div class='row'>
          <div class='input-field col s12 m6'>
            <span>Estado:</span>
            <input id='activo' type='radio' name='estado_cliente' class='with-gap' value='1' <?php print(($estado_cliente == 1)?"checked":""); ?>/>
            <label for='activo'><i class='material-icons left'>visibility</i></label>
            <input id='inactivo' type='radio' name='estado_cliente' class='with-gap' value='0' <?php print(($estado_cliente == 0)?"checked":""); ?>/>
            <label for='inactivo'><i class='material-icons left'>visibility_off</i></label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='usuario_cliente' type='text' name='usuario_cliente' class='validate' value='<?php print($usuario_cliente); ?>'/>
            <label for='usuario_cliente'>usuario</label>
        </div>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='contrasenia_cliente1' type='password' name='contrasenia_cliente1' class='validate' required/>
            <label for='contrasenia_cliente1'>Contraseña</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='contrasenia_cliente2' type='password' name='contrasenia_cliente2' class='validate' required/>
            <label for='contrasenia_cliente2'>Confirmar contraseña</label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='clientes.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>




    <?php
    require("inc/pie.php")
    ?>
</body>
</html>