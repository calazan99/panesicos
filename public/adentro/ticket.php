<?php
require("../../lib/database.php");
require("../../lib/validator.php");
include "datos.php";
?>
<html>
<meta charset="utf-8">
    <head>
        <title>Factura</title>
        <link rel="stylesheet" href="style/ticket.css">
    </head>
<body>
<br><br><br>
    <article>
        <section id="logo">
        	<img src="pictures/Logo2.png" height="25%" width="65%">
        </section>
        
        <section id ="encabezado">
        	<h1>Hot Loaves S.A DE C.V</h1>
        	<h2>Venta de emparedados estilo americano, ticket de cobro de pagina web. <br>
        		RESOLUCIÓN: 12301-NEX-A-0360-2015<br>
        	</h2>
        	<h3>
        		Sucursal <br>
        		Km 1 1/2 Calle a plan del Pino, Ciudadela Don Bosco, Soyapango. San Salvador.
        		Telefono: 2251-5000
        		<br><br>

        		<table>
        			<tr><td>Método de pago: </td><td>Contado </td></tr>
        		</table>
        		<hr>
        		<table> 
        			<tr><td>Cliente: </td><td> <?php echo $result ?> </td></tr>
        		</table>
        		<br>
        		<table>
        			<tr><td>Tipo de loave </td><td> <?php echo $nombre ?> </td></tr>
        		</table>
        		<br>
        		<table>
        			<tr><td>Precio </td><td> $<?php echo $precio ?> </td></tr>
        		</table>
        		<hr>
        		<br>
        		<p>
        		Si estas de acuerdo con tu pedido, pulsa el boton de enviar.
                Podras recoger tu pedido despues de 15 minutos de haber enviado
                el pedido. Si quieres cambiar el pedido pulsa el boton regresar.
        		</p>
                <br>
        	</h3>
            <form action="menu.php" method="post">
            <input type="submit" value="Regresar">
            </form>

            <form action="epm.php" method="post">
            <input type="submit" value="Enviar">
            </form>
        </section>

    </article>
</body>
</html>