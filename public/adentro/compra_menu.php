<?php
include "validar_pagina.php";

$nombre = $_REQUEST['pan'];

switch ($nombre) {
	case 'pollobacon-mediano':
		$precio = 4.99;
		header ("location: ticket.php");
		break;

	case 'pollobacon-grande':
		$precio = 5.99;
		header ("location: ticket.php");
		break;

	case 'pollopizziola-mediano':
		$precio = 4.99;
		header ("location: ticket.php");
		break;

	case 'pollopizziola-grande':
		$precio = 5.99;
		header ("location: ticket.php");
		break;
   
	case 'carnequeso-mediano':
		$precio = 4.99;
		header ("location: ticket.php");
		break;

	case 'carnequeso-grande':
		$precio = 5.99;
		header ("location: ticket.php");
		break;

	case 'polloteriyaki-mediano':
		$precio = 4.99;
		header ("Location: ticket.php");
		break;

	case 'polloteriyaki-grande':
		$precio = 5.99;
		header ("location: ticket.php");
		break;

	case 'roastbeef-mediano':
		$precio = 3.99;
		header ("location: ticket.php");
		break;

	case 'roastbeef-grande':
		$precio = 4.99;
		header ("location: ticket.php");
		break;

	case 'polloasado-mediano':
		$precio = 3.99;
		header ("location: ticket.php");
		break;

	case 'polloasado-grande':
		$precio = 4.99;
		header ("location: ticket.php");
		break;

	case 'hlmelt-mediano':
		$precio = 4.99;
		header ("location: ticket.php");
		break;

	case 'hlmelt-grande':
		$precio = 5.99;
		header ("location: ticket.php");
		break;

	case 'mariscos-mediano':
		$precio = 4.99;
		header ("location: ticket.php");
		break;

	case 'mariscos-grande':
		$precio = 5.99;
		header ("location: ticket.php");
		break;

	case 'pechugapavo-mediano':
		$precio = 3.99;
		header ("location: ticket.php");
		break;

	case 'pechugapavo-grande':
		$precio = 4.99;
		header ("location: ticket.php");
		break;

	case 'atun-mediano':
		$precio = 4.99;
		header ("location: ticket.php");
		break;

	case 'atun-grande':
		$precio = 5.99;
		header ("location: ticket.php");
		break;

	case 'hlclub-mediano':
		$precio = 4.99;
		header ("location: ticket.php");
		break;

	case 'hlclub-grande':
		$precio = 5.99;
		header ("location: ticket.php");
		break;

	case 'italianomelt-mediano':
		$precio = 4.99;
		header ("location: ticket.php");
		break;

	case 'italianomelt-grande':
		$precio = 5.99;
		header ("location: ticket.php");
		break;

	case 'vegetariano-mediano':
		$precio = 3.99;
		header ("location: ticket.php");
		break;

	case 'vegetariano-grande':
		$precio = 4.99;
		header ("location: ticket.php");
		break;

	case 'jamon-mediano':
		$precio = 3.99;
		header ("location: ticket.php");
		break;

	case 'jamon-grande':
		$precio = 4.99;
		header ("location: ticket.php");
		break;

	case 'italianobmt-mediano':
		$precio = 4.99;
		header ("location: ticket.php");
		break;

	case 'italianobmt-grande':
		$precio = 5.99;
		header ("location: ticket.php");
		break;

	case 'pavojamon-mediano':
		$precio = 4.99;
		header ("location: ticket.php");
		break;

	case 'pavojamon-grande':
		$precio = 5.99;
		header ("location: ticket.php");
		break;
	
	default:
		break;
}

setcookie('precio', $precio, time()+2000);
setcookie('nombre', $nombre, time()+2000);

?>
