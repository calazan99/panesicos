<?php
include "validar_pagina.php";
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Galeria</title>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<link rel="stylesheet" type="text/css" href="style/estilos.css">
<link rel="stylesheet" type="text/css" href="style/otros.css">
<link rel="stylesheet" type="text/css" href="style/responsive_menu_elegir.css">
<link rel="stylesheet" type="text/css" href="style/responsive_menu_compra.css">
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="js/javascr.js"></script>
<script type="text/javascript" src="js/otros.js"></script>
</head>

<header>
	<div class="wrapper">
			<div class="logo"><img src="pictures/Logo2.png"></div>
			<a class="ww">Elige tu Loave preferido</a>
			<nav>
				<a href="pag2.php">Inicio</a>
				<a href="cocina.php">Crea Tu Pan</a>
				<a href="main/logout.php">Cerrar sesión</a>
			</nav>
		</div>
</header>

<body>
	<!-- Galeria Inicio-->
	<div id="galeria">
	 <section>
		<a href="pan1">
			<div class="trabajos pan1">
				<p>POLLO Y BACON RANCH</p>
				<div><img src="pictures/Logo1.png" style="width:30px  height:20px"></div>
			</div>
		</a>
		<a href="pan2">
			<div class="trabajos pan2">
				<p>POLLO PIZZIOLA MELT</p>
				<div><img src="pictures/Logo1.png"></div>
			</div>
		</a>
		<a href="pan3">
			<div class="trabajos pan3">
				<p>CARNE CON QUESO</p>
				<div><img src="pictures/Logo1.png"></div>
			</div>
		</a>
		<a href="pan4">
			<div class="trabajos pan4">
				<p>POLLO ESTILO TERIYAKI</p>
				<div><img src="pictures/Logo1.png"></div>
			</div>
		</a>
		<a href="pan5">
			<div class="trabajos pan5">
				<p>ROAST BEEF</p>
				<div><img src="pictures/Logo1.png"></div>
			</div>
		</a>
		<a href="pan6">
			<div class="trabajos pan6">
				<p>POLLO ASADO</p>
				<div><img src="pictures/Logo1.png"></div>
			</div>
		</a>
		<a href="pan7">
			<div class="trabajos pan7">
				<p>HOT LOAVES MELT ™</p>
				<div><img src="pictures/Logo1.png"></div>
			</div>
		</a>
		<a href="pan8">
			<div class="trabajos pan8">
				<p>MARISCOS CON QUESO</p>
				<div><img src="pictures/Logo1.png"></div>
			</div>
		</a>
		<a href="pan9">
			<div class="trabajos pan9">
				<p>PECHUGA DE PAVO</p>
				<div><img src="pictures/Logo1.png"></div>
			</div>
		</a>
		<a href="pan10">
			<div class="trabajos pan10">
				<p>ATÚN CON QUESO</p>
				<div><img src="pictures/Logo1.png"></div>
			</div>
		</a>
		<a href="pan11">
			<div class="trabajos pan11">
				<p>HOT LOAVES CLUB ™</p>
				<div><img src="pictures/Logo1.png"></div>
			</div>
		</a>
		<a href="pan12">
			<div class="trabajos pan12">
				<p>ITALIANO PICANTE MELT</p>
				<div><img src="pictures/Logo1.png"></div>
			</div>
		</a>
		<a href="pan13">
			<div class="trabajos pan13">
				<p>DELEITE VEGETARIANO ™</p>
				<div><img src="pictures/Logo1.png"></div>
			</div>
		</a>
		<a href="pan14">
			<div class="trabajos pan14">
				<p>JAMÓN</p>
				<div><img src="pictures/Logo1.png"></div>
			</div>
		</a>
		<a href="pan15">
			<div class="trabajos pan15">
				<p>ITALIANO B.M.T.</p>
				<div><img src="pictures/Logo1.png"></div>
			</div>
		</a>
		<a href="pan16">
			<div class="trabajos pan16">
				<p>PAVO Y JAMÓN</p>
				<div><img src="pictures/Logo1.png"></div>
			</div>
		</a>
	</section>
	</div>
	<!-- Galeria Final-->
    <!-- Colecciones Inicio-->

<div id="colecciones">

		<div class="coleccion">
			<div class="imagenes" id="pan1">
				<img src="pictures/sandwich1.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>POLLO Y BACON RANCH</h3><br>
				<p>¿Qué estás esperando para probar nuestro exquisito Pollo Bacon Ranch? Apetitosa pechuga de 
				pollo con queso derretido y crujiente tocineta, recubierto con deliciosa y cremosa salsa ranch, 
				acompañada de los vegetales a tu elección, y servida sobre exquisito pan recién horneado.</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="pollobacon-mediano" type="radio" name="pan" checked>Mediano:   $4.99
                <input class="comprar" value="pollobacon-grande" type="radio" name="pan">Grande:   $5.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>				
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
		
		<div class="coleccion">
			<div class="imagenes" id="pan2">
				<img src="pictures/sandwich2.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>POLLO PIZZIOLA MELT</h3><br>
				<p>Deléitate con nuestra deliciosa receta original de Pollo Pizziola, exquisito pollo asado en 
				salsa marinara con pepperoni, queso, tomate y orégano en pan recién horneado.</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="pollopizziola-mediano" type="radio" name="pan" checked>Mediano:   $4.99
                <input class="comprar" value="pollopizziola-grande" type="radio" name="pan">Grande:   $5.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
		
		<div class="coleccion">
			<div class="imagenes" id="pan3">
				<img src="pictures/sandwich3.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>CARNE CON QUESO</h3><br>
				<p>Inspirado en una de las recetas favoritas de todos, hecho a base de carne de la más alta 
				calidad sazonada con cebolla, pimientos verdes y queso derretido que se deshace en tu boca. 
				Se sirve calientito con los vegetales y aderezos de tu elección sobre pan recién horneado.</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="carnequeso-mediano" type="radio" name="pan" checked>Mediano:   $4.99
                <input class="comprar" value="carnequeso-grande" type="radio" name="pan">Grande:   $5.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
		
		<div class="coleccion">
			<div class="imagenes" id="pan4">
				<img src="pictures/sandwich4.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>POLLO ESTILO TERIYAKI</h3><br>
				<p>Un clásico de Asia por su exquisita salsa agridulce estilo teriyaki, hecho a base de trozos 
				de pollo tierno, bañado en esta deliciosa salsa. Se sirve caliente y cubierto de tu selección 
				favorita de vegetales y salsas, sobre pan recién horneado. ¡Definitivamente un sabor que será 
				irresistible a tu paladar! No puedes dejar de probarlo.</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="polloteriyaki-mediano" type="radio" name="pan" checked>Mediano:   $4.99
                <input class="comprar" value="polloteriyaki-grande" type="radio" name="pan">Grande:   $5.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
		
		<div class="coleccion">
			<div class="imagenes" id="pan5">
				<img src="pictures/sandwich5.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>ROAST BEEF</h3><br>
				<p>Un clásico favorito hecho a base de roast beef magro y tierno, acompañado de vegetales, 
				condimentos y salsas de tu elección sobre pan recién horneado.</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="roastbeef-mediano" type="radio" name="pan" checked> Mediano:  $3.99
                <input class="comprar" value="roastbeef-grande" type="radio" name="pan">Grande:   $4.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
		
		<div class="coleccion">
			<div class="imagenes" id="pan6">
				<img src="pictures/sandwich6.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>POLLO ASADO</h3><br>
				<p>¿Te atreves a probar un pollo diferente? Deliciosopollo asado, acompañado de nuestros 
				exquisitos vegetales y aderezos. ¡Una receta diferente y de mucha tradición!</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="polloasado-mediano" type="radio" name="pan" checked>Mediano:   $3.99
                <input class="comprar" value="polloasado-grande" type="radio" name="pan">Grande:   $4.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
		
		<div class="coleccion">
			<div class="imagenes" id="pan7">
				<img src="pictures/sandwich7.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>HOT LOAVES MELT ™</h3><br>
				<p>Imagina tu pan recién horneado relleno de exquisitas rebanadas de pavo, jamón, crujiente 
				tocineta y queso derretido. ¡Mmmmm simplemente delicioso! Todo acompañado de los vegetales 
				y salsas de tu elección. Deja de imaginártelo y ve por el tuyo a tu restaurante HOT LOAVES ® 
				más cercano y ¡disfrútalo!</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="hlmelt-mediano" type="radio" name="pan" checked>Mediano:   $4.99
                <input class="comprar" value="hlmelt-grande" type="radio" name="pan"> Grande:  $5.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
		
		<div class="coleccion">
			<div class="imagenes" id="pan8">
				<img src="pictures/sandwich8.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>MARISCOS CON QUESO</h3><br>
				<p>Si eres amante de los mariscos, atrévete a probar el delicioso Sub de Mariscos con 
				nuestros deliciosos vegetales de tu selección y queso.</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="mariscos-mediano" type="radio" name="pan" checked>Mediano   $4.99
                <input class="comprar" value="mariscos-grande" type="radio" name="pan">Grande:   $5.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
		
		<div class="coleccion">
			<div class="imagenes" id="pan9">
				<img src="pictures/sandwich9.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>PECHUGA DE PAVO</h3><br>
				<p>Disfruta rico LOV de Pechuga de Pavo. Deliciosa pechuga de pavo en rebanadas estilo 
				gourmet con una selección de vegetales y condimentos en exquisito pan recién horneado.</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="pechugapavo-mediano" type="radio" name="pan" checked>Mediano:   $3.99
                <input class="comprar" value="pechugapavo-grande" type="radio" name="pan"> Grande:  $4.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
		
		<div class="coleccion">
			<div class="imagenes" id="pan10">
				<img src="pictures/sandwich10.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>ATÚN CON QUESO</h3><br>
				<p>Delicioso Atún mezclado con mayonesa, para hacer uno de los Sub más ricos del mundo. 
				El Lov de Atún está disponible con la elección de tus vegetales favoritos, especias y salsas, 
				y se sirve en pan recién horneado.</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="atun-mediano" type="radio" name="pan" checked> Mediano:  $4.99
                <input class="comprar" value="atun-grande" type="radio" name="pan"> Grande:  $5.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
		
		<div class="coleccion">
			<div class="imagenes" id="pan11">
				<img src="pictures/sandwich11.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>HOT LOAVES CLUB ™</h3><br>
				<p>¡El Hot Loaves Club ™ es un clásico de la casa! Una combinación de sabor de nuestra pechuga 
				de pavo, tierno roast beef en rebanadas y jamón, con la selección de vegetales, especias y 
				salsas de tu elección. Servido en pan gourmet recién horneado.</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="hlclub-mediano" type="radio" name="pan" checked>Mediano:   $4.99
                <input class="comprar" value="hlclub-grande" type="radio" name="pan">Grande:   $5.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
		
		<div class="coleccion">
			<div class="imagenes" id="pan12">
				<img src="pictures/sandwich12.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>ITALIANO PICANTE MELT</h3><br>
				<p>¡WOW, qué combinación! Exquisitas rebanadas de pepperoni y salami estilo Génova, con queso derretido 
				acompañado por vegetales y rico pan recién horneado. ¡Pídelo tostado!</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="italianomelt-mediano" type="radio" name="pan" checked>Mediano:   $4.99
                <input class="comprar" value="italianomelt-grande" type="radio" name="pan">Grande:   $5.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
		
		<div class="coleccion">
			<div class="imagenes" id="pan13">
				<img src="pictures/sandwich13.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>DELEITE VEGETARIANO ™</h3><br>
				<p>Una combinación de lechuga, tomates, pimentones verdes, cebolla, aceitunas y condimentos 
				de tu elección sobre pan gourmet recién horneado. Es como si fuera un LOV de ensalada, 
				¡mmm... delicioso!.</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="vegetariano-mediano" type="radio" name="pan" checked>Mediano:   $3.99
                <input class="comprar" value="vegetariano-grande" type="radio" name="pan">Grande:   $4.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
		
		<div class="coleccion">
			<div class="imagenes" id="pan14">
				<img src="pictures/sandwich14.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>JAMÓN</h3><br>
				<p>Exquisitas rebanadas de jamón sobre una cama de vegetales y salsas a tu gusto. 
				¡No te puedes perder este LOV!</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="jamon-mediano" type="radio" name="pan" checked>Mediano:   $3.99
                <input class="comprar" value="jamon-grande" type="radio" name="pan"> Grande:  $4.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
		
		<div class="coleccion">
			<div class="imagenes" id="pan15">
				<img src="pictures/sandwich15.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>ITALIANO B.M.T. ™ MELT</h3><br>
				<p>El rico sabor de Italia. Salami rebanado estilo Génova, pepperoni y jamón, 
				acompañado de vegetales y condimentos. ¡La verdad es que con sólo verlo... te antojas!</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="italianobmt-mediano" type="radio" name="pan" checked>Mediano  $4.99
                <input class="comprar" value="italianobmt-grande" type="radio" name="pan">Grande:   $5.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
		
		<div class="coleccion">
			<div class="imagenes" id="pan16">
				<img src="pictures/sandwich16.jpg" alt="">
			</div><!-- Imagenes-->
			<div class="descripcion">
				<h3>PAVO Y JAMÓN</h3><br>
				<p>¡Una combinación perfecta! Deliciosa pechuga de pavo con rebanas de jamón 
				con tu selección de verduras y deliciosas salsas, y servida sobre pan recién horneado.</p>
				<form action="compra_menu.php" method="POST">
				<input class="comprar" value="pavojamon-mediano" type="radio" name="pan" checked>Mediano:   $4.99
                <input class="comprar" value="pavojamon-grande" type="radio" name="pan"> Grande:  $5.99 <br>
                <input class="comprar" type="submit" value="Comprar">
				</form>
				<div class="cerrar">
					<a href="#">Cerrar</a>
				</div><!--Cerrar-->
			</div><!-- Descripcion-->
		</div><!-- coleccion-->
 </div><!-- colecciones-->
	<!-- Coleccion Final-->
</body>
</html>