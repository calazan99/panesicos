<?php
require("../../../lib/database.php");
require("../../../lib/validator.php");
class Page
{
	public static function header($title)
	{
		session_start();
		ini_set("date.timezone","America/El_Salvador");
		print("
			<!DOCTYPE html>
			<html lang='es'>
			<head>
				<meta charset='utf-8'>
				<title>Dashboard - $title</title>
				<link type='text/css' rel='stylesheet' href='../../../css/materialize.min.css'/>
				<link type='text/css' rel='stylesheet' href='../../../css/sweetalert2.min.css'/>
				<link type='text/css' rel='stylesheet' href='../../../css/icons.css'/>
				<link type='text/css' rel='stylesheet' href='../../css/zebra_pagination.css'/>
							
				<!-- Malihu jQuery custom content scroller CSS -->
				<link rel='stylesheet' href='../css/jquery.mCustomScrollbar.css'>
				
				<!-- Sweet Alert CSS -->
				<script type='text/javascript' src='../../../js/sweetalert2.min.js'></script>
				<meta name='viewport' content='width=device-width, initial-scale=1.0'/>
			</head>
			<body>
		");
		if(isset($_SESSION['usuario_cliente']))
		{
			print("
				
    <!-- Nav Lateral -->
    <section class='NavLateral full-width'>
        <div class='NavLateral-FontMenu full-width ShowHideMenu'></div>
        <div class='NavLateral-content full-width'>
            <header class='NavLateral-title full-width center-align'>
                Panes Icos <i class='zmdi zmdi-close NavLateral-title-btn ShowHideMenu'></i>
            </header>
            <figure class='full-width NavLateral-logo'>
                <img src='../img/logo.png' alt='material-logo' class='responsive-img center-box'>
                <figcaption class='center-align'></figcaption>
            </figure> 
            <div class='NavLateral-Nav'>
                <ul class='full-width'>
                    <li>
                        <a href='../main/index.php' class='waves-effect waves-light'><i class='zmdi zmdi-home zmdi-hc-fw'></i> Home </a>
                    </li>
                    <li class='NavLateralDivider'></li>
                    <li>
                        <a href='../main/profile.php' class='waves-effect waves-light'><i class='zmdi zmdi-account zmdi-hc-fw'></i></i> Perfil </a>
                    </li>
                    <li class='NavLateralDivider'></li>
                    <li>
                        <a href='../cliente/index.php' class='waves-effect waves-light'><i class='zmdi zmdi-accounts-alt zmdi-hc-fw'></i></i> Clientes</a>
                    </li>
                    <li class='NavLateralDivider'></li>
                    <li>
                        <a href='../ingrediente_principal/index.php' class='NavLateral-DropDown  waves-effect waves-light'><i class='zmdi zmdi-collection-case-play zmdi-hc-fw'></i> <i class='zmdi zmdi-chevron-down NavLateral-CaretDown'></i> Productos </a>
                        <ul class='full-width'>
                            <li><a href='../ingrediente_principal/index.php' class='waves-effect waves-light'>Ingredientes principales</a></li>
                            <li class='NavLateralDivider'></li>
                            <li><a href='../tipo_pan/index.php' class='waves-effect waves-light'>Tipos de panes</a></li>
                            <li class='NavLateralDivider'></li>
                            <li><a href='../tipo_queso/index.php' class='waves-effect waves-light'>Tipos de queso</a></li>
                            <li class='NavLateralDivider'></li>
                            <li><a href='../ingrediente_adicional/index.php' class='waves-effect waves-light'>Ingredientes adicionales</a></li>
                            <li class='NavLateralDivider'></li>
                            <li><a href='../ingrediente_extra/index.php' class='waves-effect waves-light'>Ingredientes extras</a></li>
                            </ul>
                    </li>
                    <li class='NavLateralDivider'></li>
                    <li>
                        <a href='../combo/index.php' class='waves-effect waves-light'><i class='zmdi zmdi-comment-alert zmdi-hc-fw'></i></i> Promociones</a>
                    </li>
					<li class='NavLateralDivider'></li>
                    <li>
                        <a href='../usuarios/index.php' class='waves-effect waves-light'><i class='zmdi zmdi-accounts-alt zmdi-hc-fw'></i></i> Usuarios </a>
                    </li>
                </ul>
            </div>  
        </div>  
    </section>
   <section class='ContentPage full-width'>
        <!-- Nav Info -->
        <div class='ContentPage-Nav full-width'>
            <ul class='full-width'>
                
                <li><figure><img src='../img/user.png' alt='UserImage'></figure></li>
                <li style='padding:0 5px;'>".$_SESSION['usuario_cliente']."</li>
				<li ><a href='main/logout.php' class='tooltipped waves-effect waves-light' data-position='bottom' data-delay='50' data-tooltip='Menu'><i class='zmdi zmdi-power'></i></a>
                
               
            </ul>   
        </div>
				<main class='container'>
					<h3 class='center-align'>".$title."</h3>
			");
		}
		else
		{
			print("
				<header class='navbar-fixed'>
					<nav class='black'>
						
					</nav>
				</header>
				<main class='container'>
			");
			$filename = basename($_SERVER['PHP_SELF']);
			if($filename != "login.php" && $filename != "register.php")
			{
				self::showMessage(3, "¡Debe  iniciar sesión!", "../main/login.php");
				self::footer();
				exit;
			}
			else
			{
				print("<h3 class='center-align'>".$title."</h3>");
			}
		}
	}

	public static function footer()
	{
		print("
			</main>
			
			<footer class='footer-MaterialDark black'>
            <div class='container'>
                <div class='row'>
                    <div class='col l6 s12'>
                        <h5 class='white-text'>Panes Icos</h5>
                        <p class='grey-text text-lighten-4'>
                            
                            <br>
                           Hay algo tan necesario como el pan de cada día, y es la paz de cada día; la paz sin la cual el mismo pan es amargo.
                             </p>
                    </div>
                    <div class='col l4 offset-l2 s12'>
                    <h5 class='white-text'>Siguenos en nuestras redes sociales</h5>
                    <div class='fb-page' data-href='https://www.facebook.com/Panes-Icos-1835152290032495/?ref=bookmarks' data-tabs='timeline' data-width='250' data-height='100' data-small-header='false' data-adapt-container-width='true' data-hide-cover='false' data-show-facepile='true'><blockquote cite='https://www.facebook.com/Panes-Icos-1835152290032495/?ref=bookmarks' class='fb-xfbml-parse-ignore'><a href='https://www.facebook.com/Panes-Icos-1835152290032495/?ref=bookmarks'>Panes Icos</a></blockquote></div>

                    </div>
                </div>
            </div>
            <div class='NavLateralDivider'></div>
            <div class='footer-copyright'>
                <div class='container center-align'>
                    © 2017 Derechos reservados de panes icos
                </div>
            </div>
        </footer>
		</section>

			<!-- facebook JS -->
				<div id='fb-root'></div>
			<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = '//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8';
			fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			<script type='text/javascript' src='../../js/jquery-2.1.1.min.js'></script>
			<script type='text/javascript' src='../../js/materialize.min.js'></script>
			<script type='text/javascript' src='../js/dashboard.js'></script>
			<!-- Sweet Alert JS -->
			
			
			

			</body>
			</html>
		");
	}

	public static function setCombo($label, $name, $value, $query)
	{
		$data = Database::getRows($query, null);
		print("<select name='$name' required>");
		if($data != null)
		{
			if($value == null)
			{
				print("<option value='' disabled selected>Seleccione una opción</option>");
			}
			foreach($data as $row)
			{
				if(isset($_POST[$name]) == $row[0] || $value == $row[0])
				{
					print("<option value='$row[0]' selected>$row[1]</option>");
				}
				else
				{
					print("<option value='$row[0]'>$row[1]</option>");
				}
			}
		}
		else
		{
			print("<option value='' disabled selected>No hay registros</option>");
		}
		print("
			</select>
			<label>$label</label>
		");
	}

	public static function showMessage($type, $message, $url)
	{
		$text = addslashes($message);
		switch($type)
		{
			case 1:
				$title = "Éxito";
				$icon = "success";
				break;
			case 2:
				$title = "Error";
				$icon = "error";
				break;
			case 3:
				$title = "Advertencia";
				$icon = "warning";
				break;
			case 4:
				$title = "Aviso";
				$icon = "info";
		}
		if($url != null)
		{
			print("<script>swal({title: '$title', text: '$text', type: '$icon', confirmButtonText: 'Aceptar', allowOutsideClick: false, allowEscapeKey: false}).then(function(){location.href = '$url'})</script>");
		}
		else
		{
			print("<script>swal({title: '$title', text: '$text', type: '$icon', confirmButtonText: 'Aceptar', allowOutsideClick: false, allowEscapeKey: false})</script>");
		}
	}
}
?>