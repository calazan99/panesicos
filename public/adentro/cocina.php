<?php
include "validar_pagina.php";
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="style/cocina.css">
	<link rel="stylesheet" href="style/otros.css">
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src='js/cocina.js'></script>
</head>
<header class="header2">
<div class="wrapper">
			<div class="logo"><img src="pictures/Logo2.png"></div>
			<nav>
				<a href="Pag2.php">Inicio</a>
				<a href="menu.php">Menú</a>
				<a href="main/logout.php">Cerrar sesión</a>
			</nav>
		</div>
</header>
<body>
	<div id="tituloaux">Escoge la especialidad del pan!</div>
	<div class="cuentaaux">cuenta</div>
	<div id="avanzaraux">Escoger Vegetales</div>
	<div id="avanzar2aux">Escoger Aderesos</div>
	<div id="avanzar3aux">Cuenta</div>
	<div id="regresar1aux">Regresar</div>
	<div id="regresar2aux">Regresar</div>
	<div id="regresar3aux">Regresar</div>
<section id="pan">
<table>
	<tr>
	<td class="img"><img id="tamaño2" src="pictures/Imagenes Pan/PanOreganoElegir.png"></td>
	<td class="normal">Oregano<p class="precio">$0.80</p></td>
	<td class="normal"><div class="button1">Agregar</div></td>
</tr>
	<tr>
	<td class="img"><img id="tamaño2" src="pictures/Imagenes Pan/PanBlancoElegir.png"></td>
	<td class="normal">Blanco<p class="precio">$0.70</p></td>
	<td class="normal"><div class="button2">Agregar</div></td>
</tr>
	<tr>
	<td class="img"><img id="tamaño2" src="pictures/Imagenes Pan/PanIntegralElegir.png"></td>
	<td class="normal">Integral<p class="precio">$0.60</p></td>
	<td class="normal"><div class="button3">Agregar</div></td>
</tr>
	<tr>
	<td class="img"><img id="tamaño2" src="pictures/Imagenes Pan/PanLightElegir.png"></td>
	<td class="normal">Light<p class="precio">$0.90</p></td>
	<td class="normal"><div class="button4">Agregar</div></td>
</tr>
</table>
</section>
<section id="jamon">
<table>
	<tr>
	<td class="img"><img id="tamaño4" src="pictures/Imagenes Pan/PavoAhumadoElegir.png"></td>
	<td class="normal">Pavo Suizo<p class="precio">$0.90</p></td>
	<td class="normal"><div class="button5">Agregar</div>
		<div class="button5e">Retirar</div></td>
</tr>
	<tr>
	<td class="img"><img id="tamaño5" src="pictures/Imagenes Pan/PechugaPavoElegir.png"></td>
	<td class="normal">Pechuga de Pavo<p class="precio">$0.90</p></td>
	<td class="normal"><div class="button6">Agregar</div>
		<div class="button6e">Retirar</div></td>
</tr>
	<tr>
	<td class="img" ><img id="tamaño6" src="pictures/Imagenes Pan/JamonSerranoElegir.png"></td>
	<td class="normal">Serrano<p class="precio">$0.90</p></td>
	<td class="normal"><div class="button7">Agregar</div>
		<div class="button7e">Retirar</div></td>
</tr>
	<tr>
	
	<td class="img"><img id="tamaño5" src="pictures/Imagenes Pan/JamonYorkElegir.png"></td>
	<td class="normal">York<p class="precio">$0.90</p></td>
	<td class="normal"><div class="button8">Agregar</div>
		<div class="button8e">Retirar</div></td>
</tr>
</table>
</section>
<section id="vegetales">
<table>
	<tr>
	<td class="img"><img id="tamaño7" src="pictures/Imagenes Pan/PepinoElegir.png"></td>
	<td class="normal">Pepino<p class="precio">$0.10</p></td>
	<td class="normal"><div class="button9">Agregar</div>
		<div class="button9e">Retirar</div></td>
</tr>
	<tr>
	<td class="img"><img id="tamaño8" src="pictures/Imagenes Pan/Tomate Elegir.png"></td>
	<td class="normal">Tomate<p class="precio">$0.15</p></td>
	<td class="normal"><div class="button10">Agregar</div>
		<div class="button10e">Retirar</div></td>
</tr>
	<tr>
	<td class="img"><img id="tamaño7" src="pictures/Imagenes Pan/PimientoVerdeElegir.png"></td>
	<td class="normal">Chile Verde<p class="precio">$0.10</p></td>
	<td class="normal"><div class="button11">Agregar</div>
		<div class="button11e">Retirar</div></td>
</tr>
	<tr>
	<td class="img"><img id="tamaño42" src="pictures/Imagenes Pan/Aceitunas Elegir.png"></td>
	<td class="normal">Aceitunas<p class="precio">$0.20</p></td>
	<td class="normal"><div class="button12">Agregar</div>
		<div class="button12e">Retirar</div></td>
</tr>
<tr>
	<td class="img"><img id="tamaño9" src="pictures/Imagenes Pan/CebollaElegir.png"></td>
	<td class="normal">Cebolla<p class="precio">$0.15</p></td>
	<td class="normal"><div class="button13">Agregar</div>
	<div class="button13e">Retirar</div></td>
</tr>
</table>
</section>
<section id="aderesos">
<table>
	<tr>
	<td class="img"><img id="tamaño82" src="pictures/Imagenes Pan/KetchupElegir.png"></td>
	<td class="normal">Ketchup<p class="precio">$0.05</p></td>
	<td class="normal"><div class="button14">Agregar</div>
		<div class="button14e">Retirar</div></td>
</tr>
	<tr>
	<td class="img"><img id="tamaño82" src="pictures/Imagenes Pan/MayonesaElegir.png"></td>
	<td class="normal">Mayonesa<p class="precio">$0.05</p></td>
	<td class="normal"><div class="button15">Agregar</div>
		<div class="button15e">Retirar</div></td>
</tr>
	<tr>
	<td class="img"><img id="tamaño82" src="pictures/Imagenes Pan/MostazaElegir.png"></td>
	<td class="normal">Mostaza<p class="precio">$0.05</p></td>
	<td class="normal"><div class="button16">Agregar</div>
		<div class="button16e">Retirar</div></td>
</tr>
	<tr>
	<td class="img"><img id="tamaño82" src="pictures/Imagenes Pan/ChileElegir.png"></td>
	<td class="normal">Chile<p class="precio">$0.05</p></td>
	<td class="normal"><div class="button17">Agregar</div>
		<div class="button17e">Retirar</div></td>
</tr>
</table>
</section>
<div class="on" id="bienvenida"><p>Comenzemos a crear nuestro <br>loave!</p></div>
<div class="cuenta"></div>
<div id="titulo">Escoge la especialidad del pan!</div>
<div class="holi" id="avanzar">Escoger vegetales</div>
<div class="holi" id="avanzar2">Escoger aderesos</div>
<div id="avanzar3">Cuenta</div>
<div class="holi" id="regresar1">Regresar</div>
<div class="holi" id="regresar2">Regresar</div>
<div class="holi" id="regresar3">Regresar</div>
<div class="sombraoff" id="orden">
<div class="aderezoselec1"></div>
<div class="aderezoselec2"></div>
<div class="aderezoselec3"></div>
<div class="aderezoselec4"></div>
<div class="vegetalselec1"></div>
<div class="vegetalselec2"></div>
<div class="vegetalselec3"></div>
<div class="vegetalselec4"></div>
<div class="vegetalselec5"></div>
<div class="jamonselec1"></div>
<div class="jamonselec2"></div>
<div class="jamonselec3"></div>
<div class="jamonselec4"></div>
<div class="panselec"></div>
	<div class="plato"></div>
</div>

	</body>
	</html>