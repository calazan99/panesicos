<?php
// mandar a llamaar a las librerias
require("../lib/page.php");
Page::header("Editar perfil");

if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$nombre_cliente = $_POST['nombre_cliente'];
  	$apellido_cliente = $_POST['apellido_cliente'];
    $direccion_cliente = $_POST['direccion_cliente'];
    $usuario_cliente = $_POST['usuario_cliente'];
    $telefono_cliente = $_POST['telefono_cliente'];
    $estado_cliente = $_POST['estado_cliente'];
    $contrasenia1 = $_POST['contrasenia_cliente1'];
    $contrasenia2 = $_POST['contrasenia_cliente2'];

    try 
    {
      	if($nombre_cliente != "" && $apellido_cliente != "")
        {
            if($direccion_cliente != "" && $telefono_cliente != "" )
            {
                if($usuario_cliente != "")
                {
                    if($contrasenia_cliente1 != "" || $contrasenia_cliente2 != "")
                    {
                        if($contrasenia_cliente1 == $contrasenia_cliente2)
                        {
                            $contrasenia = password_hash($contrasenia1, PASSWORD_DEFAULT);
                            $sql = "UPDATE clientes SET nombre_cliente = ?, apellido_cliente = ?, direccion_cliente = ?, usuario_cliente = ?, contrasenia_cliente = ? WHERE id_cliente = ?";
                            $params = array($nombre_cliente, $apellido_cliente, $direccion_cliente, $usuario_cliente, $contrasenia_cliente, $_SESSION['id_cliente']);
                        }
                        else
                        {
                            throw new Exception("Las contraseñas no coinciden");
                        }
                    }
                    else
                    {
                        $sql = "UPDATE clientes SET nombre_cliente = ?, apellido_cliente = ?, direccion_cliente = ?, usuario_cliente = ? WHERE id_cliente = ?";
                        $params = array($nombre_cliente, $apellido_cliente, $direccion_cliente, $cliente, $_SESSION['id_cliente']);
                    }
                    Database::executeRow($sql, $params);
                    Page::showMessage(1, "Operación satisfactoria", "index.php");
                }
                else
                {
                    throw new Exception("Debe ingresar un cliente");
                }
            }
            else
            {
                throw new Exception("Debe ingresar un correo_cliente electrónico");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
else
{
    $sql = "SELECT * FROM clientes WHERE id_cliente = ?";
    $params = array($_SESSION['id_cliente']);
    $data = Database::getRow($sql, $params);
    $nombre_cliente = $data['nombre_cliente'];
    $apellido_cliente = $data['apellido_cliente'];
    $correo_cliente = $data['correo_cliente'];
    $cliente = $data['cliente'];
}
?>

<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombre_cliente' type='text' name='nombre_cliente' class='validate' value='<?php print($nombre_cliente); ?>' required/>
          	<label for='nombre_cliente'>nombre_cliente</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellido_cliente' type='text' name='apellido_cliente' class='validate' value='<?php print($apellido_cliente); ?>' required/>
            <label for='apellido_cliente'>apellido_cliente</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input id='correo_cliente' type='email' name='correo_cliente' class='validate' value='<?php print($correo_cliente); ?>' required/>
            <label for='correo_cliente'>correo_cliente</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='cliente' type='text' name='cliente' class='validate' value='<?php print($cliente); ?>' required/>
            <label for='cliente'>cliente</label>
        </div>
    </div>
    <div class='row center-align'>
        <label>CAMBIAR contrasenia</label>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='contrasenia1' type='password' name='contrasenia1' class='validate'/>
            <label for='contrasenia1'>Contraseña nueva</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='contrasenia2' type='password' name='contrasenia2' class='validate'/>
            <label for='contrasenia2'>Confirmar contraseña</label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='../main/index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>