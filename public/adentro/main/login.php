<?php
// mandar a llamaar a las librerias
require("../lib/page.php");
Page::header("Iniciar sesión");
ob_start();
$sql = "SELECT * FROM clientes";
$data = Database::getRows($sql, null);  
if($data == null)
{
    header("location: register.php");
}

if(!empty($_POST))
{
	$_POST = validator::validateForm($_POST);
  	$usuario_cliente = $_POST['usuario_cliente'];
  	try
    {
      	if($usuario_cliente != "")
  		{
  			$sql = "SELECT * FROM clientes WHERE usuario_cliente = ?";
		    $params = array($usuario_cliente);
		    $data = Database::getRow($sql, $params);
			$contrasenia_cliente = $_POST['contrasenia_cliente'];
		    if($data != null)
		    {
		    	$hash = $data['contrasenia_cliente'];
		    	if(password_verify($contrasenia_cliente, $hash)) 
		    	{
			    	$_SESSION['id_cliente'] = $data['id_cliente'];
			      	$_SESSION['usuario_cliente'] = $data['nombre_cliente']." ".$data['apellido_cliente'];
			      	header("location:../pag2.php");
				}
				else 
				{
					throw new Exception("La contrasenia ingresada es incorrecta");
				}
		    }
		    else
		    {
		    	throw new Exception("El usuario ingresado no existe");
		    }
	  	}
	  	else
	  	{
	    	throw new Exception("Debe ingresar un usuario y una contrasenia");
	  	}
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>

<form method='post'>
	<div class='row'>
		<div class='input-field col s12 m6 offset-m3'>
			<i class='material-icons prefix'>person_pin</i>
			<input id='usuario_cliente' type='text' name='usuario_cliente' class='validate' required/>
	    	<label for='usuario'>Usuario</label>
		</div>
		<div class='input-field col s12 m6 offset-m3'>
			<i class='material-icons prefix'>security</i>
			<input id='contrasenia_cliente' type='password' name='contrasenia_cliente' class="validate" required/>
			<label for='contrasenia'>Contraseña</label>
		</div>
	</div>
	<div class='row center-align'>
		<button type='submit' class='btn waves-effect'><i class='material-icons'>send</i></button>
	</div>
</form>

<?php
Page::footer();
?>