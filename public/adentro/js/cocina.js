$(document).ready(function(){
	var pan_oregano = "";
	var pan_blanco = "";
	var pan_integral = "";
	var pan_light = "";
	var jamon_suizo = "";
	var jamon_serrano = "";
	var pechuga_depollo = "";
	var jamon_york = "";
	var pepino = "";
	var tomate = "";
	var pimiento = "";
	var aceituna = "";
	var cebolla = "";
	var ketchup = "";
	var mostaza = "";
	var mayonesa = "";
	var chile = "";
	function change(){
		if($('div#bienvenida').hasClass('on')){
			$('div#bienvenida').css({"opacity":"0"})
			$('section#pan').css({"margin-left":"2%","opacity":"1"})
			$('div#avanzar').css({"opacity":"1"})
			$('div#avanzar2').css({"opacity":"1"})
			$('div#avanzar3').css({"opacity":"1"})
			$('div#anterior').css({"opacity":"1"})
			$('div#regresar1').css({"opacity":"1"})
			$('div#regresar2').css({"opacity":"1"})
			$('div#regresar3').css({"opacity":"1"})
		}
	}
	setInterval(change, 1500)
	function change2(){
		if($('div#bienvenida').hasClass('on')){
			$('div#titulo').css({"margin-left":"40%"})
			$('div#tituloaux').css({"opacity":"1"})
			$('div#bienvenida').css({"overflow":"hidden"})
		}
	}
	setInterval(change2, 3000)
	function change3(){
		if($('div#bienvenida').hasClass('on')){
			$('div#titulo').css({"opacity":"1"})
		}
	}
	setInterval(change3, 2700)
	var cuenta;
	valor1 = 0
	valor2= 0
	valor3 =0
	valor4 = 0
	resultado = 0
	alternativa = 0
	$("div.button1").click(function(){

		$("div.panselec").css({"background-image":"url(pictures/PanOreganoAbajo.png)"})
		$("div.cuenta").css({"margin-left":"88%"})
		$("div.cuentaaux").css({"opacity":"1"})
        $("section#jamon").css({"margin-left":"2%","opacity":"1","z-index":"6"})
        $("div#avanzar").css({"width":"14%"})
        $("div#avanzaraux").css({"opacity":"1","z-index":"3"})
        $("div.cuenta").css({"width":"14%"})
        $("div#regresar1").css({"width":"14%"})
        $("div#regresar1aux").css({"opacity":"1","z-index":"3"})
        $("div#titulo").html("Escoge la especialidad del jamon!")
        $("div#tituloaux").html("Escoge la especialidad del jamon!")
        function change4(){
		if($('div#regresar1').hasClass('holi')){
			$('div#regresar1').css({"z-index":"1"})
		}
	}
	setInterval(change4, 2000)
		pan = 0.80;
		cuenta = resultado + pan
		resultado = cuenta
		valor1 = cuenta
		valor2 = valor1
		pan_oregano = "Pan oregano, "
        $("div.cuenta").html("$ "+cuenta)
        $("div.cuentaaux").html("$ "+cuenta)
		
	})
    $("div.button2").click(function(){

		$("div.panselec").css({"background-image":"url(pictures/PanBlancoAbajo.png)"})
        $("div.cuenta").css({"margin-left":"88%"})
        $("div.cuentaaux").css({"opacity":"1"})
        $("section#jamon").css({"margin-left":"2%","opacity":"1","z-index":"6"})
        $("div#avanzar").css({"width":"14%"})
        $("div#avanzaraux").css({"opacity":"1","z-index":"3"})
        $("div#regresar1aux").css({"opacity":"1","z-index":"3"})
        $("div.cuenta").css({"width":"14%"})
        $("div#regresar1").css({"width":"14%"})
        $("div#titulo").html("Escoge la especialidad del jamon!")
        $("div#tituloaux").html("Escoge la especialidad del jamon!")
        function change4(){
		if($('div#regresar1').hasClass('holi')){
			$('div#regresar1').css({"z-index":"1"})
		}
	}
	setInterval(change4, 2000)
		pan = 0.70;
		cuenta = resultado + pan
		resultado = cuenta
		valor1 = cuenta
		valor2 = valor1
		pan_blanco = "Pan blanco, "
        $("div.cuenta").html("$ "+cuenta)	
        $("div.cuentaaux").html("$ "+cuenta)	
	})

    $("div.button3").click(function(){

		$("div.panselec").css({"background-image":"url(pictures/PanIntegralAbajo.png)"})
        $("div.cuenta").css({"margin-left":"88%"})
        $("div.cuentaaux").css({"opacity":"1"})
        $("section#jamon").css({"margin-left":"2%","opacity":"1","z-index":"6"})
        $("div#avanzar").css({"width":"14%"})
        $("div.cuenta").css({"width":"14%"})
        $("div#regresar1").css({"width":"14%"})
        $("div#avanzaraux").css({"opacity":"1","z-index":"3"})
        $("div#regresar1aux").css({"opacity":"1","z-index":"3"})
        $("div#titulo").html("Escoge la especialidad del jamon!")
        $("div#tituloaux").html("Escoge la especialidad del jamon!")
        function change4(){
		if($('div#regresar1').hasClass('holi')){
			$('div#regresar1').css({"z-index":"1"})
		}
	}
	setInterval(change4, 2000)
		pan = 0.60;
		cuenta = resultado + pan
		resultado = cuenta
		valor1 = cuenta
		valor2 = valor1
		pan_integral = "Pan integral, "
        $("div.cuenta").html("$ "+cuenta)
        $("div.cuentaaux").html("$ "+cuenta)	
	})
    $("div.button4").click(function(){

		$("div.panselec").css({"background-image":"url(pictures/PanLightAbajo.png)"})
		$("div.cuenta").css({"margin-left":"88%"})
		$("div.cuentaaux").css({"opacity":"1"})
		$("section#jamon").css({"margin-left":"2%","opacity":"1","z-index":"6"})
		$("div#avanzar").css({"width":"14%"})
        $("div.cuenta").css({"width":"14%"})
        $("div#regresar1").css({"width":"14%"})
        $("div#avanzaraux").css({"opacity":"1","z-index":"3"})
        $("div#regresar1aux").css({"opacity":"1","z-index":"3"})
		$("div#titulo").html("Escoge la especialidad del jamon!")
		$("div#tituloaux").html("Escoge la especialidad del jamon!")
		function change4(){
		if($('div#regresar1').hasClass('holi')){
			$('div#regresar1').css({"z-index":"1"})
		}
	}
	setInterval(change4, 2000)
		pan = 0.90;
		cuenta = resultado + pan
		resultado = cuenta
		valor1 = cuenta
		valor2 = valor1
		pan_light = "Pan Light, "
        $("div.cuenta").html("$ "+cuenta)
        $("div.cuentaaux").html("$ "+cuenta)
	})
	$("div#regresar1").click(function(){
        $("div.jamonselec1").css({"background-image":"none"})
		$("div.jamonselec2").css({"background-image":"none"})
		$("div.jamonselec3").css({"background-image":"none"})
		$("div.jamonselec4").css({"background-image":"none"})
		$("div.button5").css({"opacity":"1", "z-index":"2"})
        $("div.button5e").css({"opacity":"0", "z-index":"1"})
        $("div.button6").css({"opacity":"1", "z-index":"2"})
        $("div.button6e").css({"opacity":"0", "z-index":"1"})
        $("div.button7").css({"opacity":"1", "z-index":"2"})
        $("div.button7e").css({"opacity":"0", "z-index":"1"})
        $("div.button8").css({"opacity":"1", "z-index":"2"})
        $("div.button8e").css({"opacity":"0", "z-index":"1"})
		$("div.panselec").css({"background-image":"none"})
		$("section#jamon").css({"margin-left":"58%","opacity":"0","z-index":"3"})
		$("div#avanzar").css({"width":"0%"})
		$("div#regresar1").css({"width":"0%"})
		$("div#titulo").html("Escoge la especialidad del pan!")
        cuenta = cuenta - resultado
        resultado = cuenta
        pan_oregano = ""
        pan_integral = ""
        pan_light = ""
        pan_blanco = ""
        jamon_york = ""
        jamon_serrano = ""
        jamon_suizo = ""
        pechuga_depollo = ""

        $("div.cuenta").html("$ "+cuenta)
        $("div.cuentaaux").html("$ "+cuenta)
	})
$("div#regresar1aux").click(function(){
        $("div.jamonselec1").css({"background-image":"none"})
		$("div.jamonselec2").css({"background-image":"none"})
		$("div.jamonselec3").css({"background-image":"none"})
		$("div.jamonselec4").css({"background-image":"none"})
		$("div.button5").css({"opacity":"1", "z-index":"2"})
        $("div.button5e").css({"opacity":"0", "z-index":"1"})
        $("div.button6").css({"opacity":"1", "z-index":"2"})
        $("div.button6e").css({"opacity":"0", "z-index":"1"})
        $("div.button7").css({"opacity":"1", "z-index":"2"})
        $("div.button7e").css({"opacity":"0", "z-index":"1"})
        $("div.button8").css({"opacity":"1", "z-index":"2"})
        $("div.button8e").css({"opacity":"0", "z-index":"1"})
		$("div.panselec").css({"background-image":"none"})
		$("section#jamon").css({"margin-left":"58%","opacity":"0","z-index":"3"})
		$("div#avanzar").css({"width":"0%"})
		$("div#regresar1").css({"width":"0%"})
		$("div#avanzaraux").css({"opacity":"0","z-index":"2"})
		$("div#regresar1aux").css({"opacity":"0","z-index":"2"})
		$("div#titulo").html("Escoge la especialidad del pan!")
        cuenta = cuenta - resultado
        resultado = cuenta
        pan_oregano = ""
        pan_integral = ""
        pan_light = ""
        pan_blanco = ""
        jamon_york = ""
        jamon_serrano = ""
        jamon_suizo = ""
        pechuga_depollo = ""

        $("div.cuenta").html("$ "+cuenta)
        $("div.cuentaaux").html("$ "+cuenta)
	})
	$("div.button5").click(function(){

		$("div.jamonselec1").css({"background-image":"url(pictures/PavoAhumeado.png)"})
		

		jamon = 0.90;
		alternativa = alternativa + jamon
		cuenta = resultado + jamon 
		resultado = cuenta
		valor2 = valor2 + jamon
		jamon_suizo = "Jamon Suizo, "
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button5").css({"opacity":"0", "z-index":"1"})
        $("div.button5e").css({"opacity":"1", "z-index":"2"})
	})
	$("div.button5e").click(function(){

		$("div.jamonselec1").css({"background-image":"none"})
		

		jamon = 0.90;
		alternativa = alternativa - jamon
		cuenta = resultado - jamon 
		resultado = cuenta
		valor2 = valor2 - jamon
		jamon_suizo = ""
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button5").css({"opacity":"1", "z-index":"2"})
        $("div.button5e").css({"opacity":"0", "z-index":"1"})
	})
	$("div.button6").click(function(){

		$("div.jamonselec2").css({"background-image":"url(pictures/PechugaPavo.png)"})

		jamon = 0.90;
		alternativa = alternativa + jamon
		cuenta = resultado + jamon
		resultado = cuenta
		valor2 = valor2 + jamon
		pechuga_depollo = "Pechuga de pavo, "
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button6").css({"opacity":"0", "z-index":"1"})
        $("div.button6e").css({"opacity":"1", "z-index":"2"})
	})
	$("div.button6e").click(function(){

		$("div.jamonselec2").css({"background-image":"none"})
		

		jamon = 0.90;
		alternativa = alternativa - jamon
		cuenta = resultado - jamon 
		resultado = cuenta
		valor2 = valor2 - jamon
		pechuga_depollo = ""
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button6").css({"opacity":"1", "z-index":"2"})
        $("div.button6e").css({"opacity":"0", "z-index":"1"})
	})
	$("div.button7").click(function(){

		$("div.jamonselec3").css({"background-image":"url(pictures/JamonSerrano.png)"})

		jamon = 0.90;
		alternativa = alternativa + jamon
		cuenta = resultado + jamon
		resultado = cuenta
		valor2 = valor2 + jamon
		jamon_serrano = "Jamon Serrano, "
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button7").css({"opacity":"0", "z-index":"1"})
        $("div.button7e").css({"opacity":"1", "z-index":"2"})
	})
	$("div.button7e").click(function(){

		$("div.jamonselec3").css({"background-image":"none"})
		

		jamon = 0.90;
		alternativa = alternativa - jamon
		cuenta = resultado - jamon 
		resultado = cuenta
		valor2 = valor2 - jamon
		jamon_serrano = ""
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button7").css({"opacity":"1", "z-index":"2"})
        $("div.button7e").css({"opacity":"0", "z-index":"1"})
	})
	$("div.button8").click(function(){

		$("div.jamonselec4").css({"background-image":"url(pictures/JamonYork.png)"})

		jamon = 0.90;
		alternativa = alternativa + jamon
		cuenta = resultado + jamon
		resultado = cuenta
		valor2 = valor2 + jamon
		jamon_york = "Jamon york, "
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button8").css({"opacity":"0", "z-index":"1"})
        $("div.button8e").css({"opacity":"1", "z-index":"2"})
	})
    $("div.button8e").click(function(){

		$("div.jamonselec4").css({"background-image":"none"})
		

		jamon = 0.90;
		alternativa = alternativa - jamon
		cuenta = resultado - jamon 
		resultado = cuenta
		valor2 = valor2 - jamon
		jamon_york = ""
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button8").css({"opacity":"1", "z-index":"2"})
        $("div.button8e").css({"opacity":"0", "z-index":"1"})
	})
	$("div#avanzar").click(function(){
		valor3 = cuenta
		$("div#avanzar").addClass('holi')
		$("div#avanzar").css({"width":"0%"})
		$("div#avanzaraux").css({"opacity":"0","z-index":"2"})
		$("section#vegetales").css({"margin-left":"2%","opacity":"1","z-index":"7"})
		$("div#regresar1").css({"width":"0%"})
		$("div#regresar1aux").css({"opacity":"0","z-index":"2"})
		$("div#titulo").html("Escoge los vegetales que desees!")
		$("div#tituloaux").html("Escoge los vegetales que desees!")
		function change4(){
		if($("div#avanzar").hasClass('holi')){
			$("div#avanzar2").css({"width":"14%"})
			$("div#avanzar2aux").css({"opacity":"1","z-index":"3"})
			$("div#avanzar").removeClass('holi')
            $("div#regresar2").css({"width":"14%"})
            $("div#regresar2aux").css({"opacity":"1","z-index":"3"})
		}
	}
	setInterval(change4, 2000)
	})
	$("div#avanzaraux").click(function(){
		valor3 = cuenta
		$("div#avanzar").addClass('holi')
		$("div#avanzar").css({"width":"0%"})
		$("div#avanzaraux").css({"opacity":"0","z-index":"2"})
		$("section#vegetales").css({"margin-left":"2%","opacity":"1","z-index":"7"})
		$("div#regresar1").css({"width":"0%"})
		$("div#regresar1aux").css({"opacity":"0","z-index":"2"})
		$("div#titulo").html("Escoge los vegetales que desees!")
		$("div#tituloaux").html("Escoge los vegetales que desees!")
		function change44(){
		if($("div#avanzar").hasClass('holi')){
			$("div#avanzar2").css({"width":"14%"})
			$("div#avanzar2aux").css({"opacity":"1","z-index":"3"})
			$("div#avanzar").removeClass('holi')
            $("div#regresar2").css({"width":"14%"})
            $("div#regresar2aux").css({"opacity":"1","z-index":"3"})
		}
	}
	setInterval(change44, 2000)
	})
	$("div#regresar2").click(function(){
		$("section#vegetales").css({"margin-left":"58%","opacity":"0","z-index":"2"})
		$("div.jamonselec1").css({"background-image":"none"})
		$("div.jamonselec2").css({"background-image":"none"})
		$("div.jamonselec3").css({"background-image":"none"})
		$("div.jamonselec4").css({"background-image":"none"})
		$("div.vegetalselec1").css({"background-image":"none"})
		$("div.vegetalselec2").css({"background-image":"none"})
		$("div.vegetalselec3").css({"background-image":"none"})
		$("div.vegetalselec4").css({"background-image":"none"})
		$("div.vegetalselec5").css({"background-image":"none"})
		$("div.button5").css({"opacity":"1", "z-index":"2"})
        $("div.button5e").css({"opacity":"0", "z-index":"1"})
        $("div.button6").css({"opacity":"1", "z-index":"2"})
        $("div.button6e").css({"opacity":"0", "z-index":"1"})
        $("div.button7").css({"opacity":"1", "z-index":"2"})
        $("div.button7e").css({"opacity":"0", "z-index":"1"})
        $("div.button8").css({"opacity":"1", "z-index":"2"})
        $("div.button8e").css({"opacity":"0", "z-index":"1"})
        $("div.button9").css({"opacity":"1", "z-index":"2"})
        $("div.button9e").css({"opacity":"0", "z-index":"1"})
        $("div.button10").css({"opacity":"1", "z-index":"2"})
        $("div.button10e").css({"opacity":"0", "z-index":"1"})
        $("div.button11").css({"opacity":"1", "z-index":"2"})
        $("div.button11e").css({"opacity":"0", "z-index":"1"})
        $("div.button12").css({"opacity":"1", "z-index":"2"})
        $("div.button12e").css({"opacity":"0", "z-index":"1"})
        $("div.button13").css({"opacity":"1", "z-index":"2"})
        $("div.button13e").css({"opacity":"0", "z-index":"1"})
		$("div#avanzar").css({"width":"14%"})
		$("div#avanzaraux").css({"opacity":"1", "z-index":"3"})
		$("div#avanzar2").css({"width":"0%"})
		$("div#avanzar2aux").css({"opacity":"0", "z-index":"2"})
		$("div#regresar2").css({"width":"0%"})
		$("div#regresar2aux").css({"opacity":"0", "z-index":"2"})
		$("div#regresar1").css({"width":"14%"})
		$("div#regresar1aux").css({"opacity":"1", "z-index":"3"})
		cuenta = valor1
		jamon_york = ""
		jamon_serrano = ""
		jamon_suizo = ""
		pechuga_depollo = ""
		pepino = ""
		cebolla = ""
		pimiento = ""
		aceituna = ""
		tomate = ""
		function change7(){
		if($("div#avanzar").hasClass('holi')){
			$("div#avanzar2").css({"width":"12%"})
			$("div#avanzar2aux").css({"opacity":"1", "z-index":"3"})
			$("div#avanzar").removeClass('holi')
            $("div#regresar2").css({"width":"12%"})
            $("div#regresar2aux").css({"opacity":"1", "z-index":"3"})
		}
	}
	setInterval(change7, 2000)
	cuenta = valor1
	resultado = cuenta
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
	})
$("div#regresar2aux").click(function(){
		$("section#vegetales").css({"margin-left":"58%","opacity":"0","z-index":"2"})
		$("div.jamonselec1").css({"background-image":"none"})
		$("div.jamonselec2").css({"background-image":"none"})
		$("div.jamonselec3").css({"background-image":"none"})
		$("div.jamonselec4").css({"background-image":"none"})
		$("div.vegetalselec1").css({"background-image":"none"})
		$("div.vegetalselec2").css({"background-image":"none"})
		$("div.vegetalselec3").css({"background-image":"none"})
		$("div.vegetalselec4").css({"background-image":"none"})
		$("div.vegetalselec5").css({"background-image":"none"})
		$("div.button5").css({"opacity":"1", "z-index":"2"})
        $("div.button5e").css({"opacity":"0", "z-index":"1"})
        $("div.button6").css({"opacity":"1", "z-index":"2"})
        $("div.button6e").css({"opacity":"0", "z-index":"1"})
        $("div.button7").css({"opacity":"1", "z-index":"2"})
        $("div.button7e").css({"opacity":"0", "z-index":"1"})
        $("div.button8").css({"opacity":"1", "z-index":"2"})
        $("div.button8e").css({"opacity":"0", "z-index":"1"})
        $("div.button9").css({"opacity":"1", "z-index":"2"})
        $("div.button9e").css({"opacity":"0", "z-index":"1"})
        $("div.button10").css({"opacity":"1", "z-index":"2"})
        $("div.button10e").css({"opacity":"0", "z-index":"1"})
        $("div.button11").css({"opacity":"1", "z-index":"2"})
        $("div.button11e").css({"opacity":"0", "z-index":"1"})
        $("div.button12").css({"opacity":"1", "z-index":"2"})
        $("div.button12e").css({"opacity":"0", "z-index":"1"})
        $("div.button13").css({"opacity":"1", "z-index":"2"})
        $("div.button13e").css({"opacity":"0", "z-index":"1"})
		$("div#avanzar").css({"width":"14%"})
		$("div#avanzaraux").css({"opacity":"1", "z-index":"3"})
		$("div#avanzar2").css({"width":"0%"})
		$("div#avanzar2aux").css({"opacity":"0", "z-index":"2"})
		$("div#regresar2").css({"width":"0%"})
		$("div#regresar2aux").css({"opacity":"0", "z-index":"2"})
		$("div#regresar1").css({"width":"14%"})
		$("div#regresar1aux").css({"opacity":"1", "z-index":"3"})
		cuenta = valor1
		jamon_york = ""
		jamon_serrano = ""
		jamon_suizo = ""
		pechuga_depollo = ""
		pepino = ""
		cebolla = ""
		pimiento = ""
		aceituna = ""
		tomate = ""
		function change77(){
		if($("div#avanzar").hasClass('holi')){
			$("div#avanzar2").css({"width":"12%"})
			$("div#avanzar2aux").css({"opacity":"1", "z-index":"3"})
			$("div#avanzar").removeClass('holi')
            $("div#regresar2").css({"width":"12%"})
            $("div#regresar2aux").css({"opacity":"1", "z-index":"3"})
		}
	}
	setInterval(change77, 2000)
	cuenta = valor1
	resultado = cuenta
        $("div.cuenta").html("$ "+cuenta)
        $("div.cuentaaux").html("$ "+cuenta)
	})
	$("div.button9").click(function(){

		$("div.vegetalselec1").css({"background-image":"url(pictures/pepino.png)"})

		vegetal = 0.10;
		cuenta = resultado + vegetal
		resultado = cuenta
		valor3 = valor3 + vegetal
		pepino = "Pepino, "
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button9").css({"opacity":"0", "z-index":"1"})
        $("div.button9e").css({"opacity":"1", "z-index":"2"})

	})
	$("div.button9e").click(function(){

		$("div.vegetalselec1").css({"background-image":"none"})
		

		vegetal = 0.10;
		alternativa = alternativa - vegetal
		cuenta = resultado - vegetal 
		resultado = cuenta
		valor3 = valor3 - vegetal
		pepino = ""
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button9").css({"opacity":"1", "z-index":"2"})
        $("div.button9e").css({"opacity":"0", "z-index":"1"})
	})
	$("div.button10").click(function(){

		$("div.vegetalselec2").css({"background-image":"url(pictures/toomate.png)"})

		vegetal = 0.15;
		cuenta = resultado + vegetal
		resultado = cuenta
		valor3 = valor3 + vegetal
		tomate = "Tomate, "
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button10").css({"opacity":"0", "z-index":"1"})
        $("div.button10e").css({"opacity":"1", "z-index":"2"})
        
	})
	$("div.button10e").click(function(){

		$("div.vegetalselec2").css({"background-image":"none"})
		

		vegetal = 0.15;
		alternativa = alternativa - vegetal
		cuenta = resultado - vegetal 
		resultado = cuenta
		valor3 = valor3 - vegetal
		tomate = ""
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button10").css({"opacity":"1", "z-index":"2"})
        $("div.button10e").css({"opacity":"0", "z-index":"1"})
	})
	$("div.button11").click(function(){

		$("div.vegetalselec3").css({"background-image":"url(pictures/piimiento.png)"})

		vegetal = 0.10;
		cuenta = resultado + vegetal
		resultado = cuenta
		valor3 = valor3 + vegetal
		pimiento = "Pimiento, "
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button11").css({"opacity":"0", "z-index":"1"})
        $("div.button11e").css({"opacity":"1", "z-index":"2"})
        
	})
	$("div.button11e").click(function(){

		$("div.vegetalselec3").css({"background-image":"none"})
		

		vegetal = 0.10;
		alternativa = alternativa - vegetal
		cuenta = resultado - vegetal 
		resultado = cuenta
		valor3 = valor3 - vegetal
		pimiento = ""
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button11").css({"opacity":"1", "z-index":"2"})
        $("div.button11e").css({"opacity":"0", "z-index":"1"})
	})
	$("div.button12").click(function(){

		$("div.vegetalselec4").css({"background-image":"url(pictures/aceeituna.png)"})

		vegetal = 0.20;
		cuenta = resultado + vegetal
		resultado = cuenta
		valor3 = valor3 + vegetal
		aceituna = "Aceituna, "
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button12").css({"opacity":"0", "z-index":"1"})
        $("div.button12e").css({"opacity":"1", "z-index":"2"})
        
	})
	$("div.button12e").click(function(){

		$("div.vegetalselec4").css({"background-image":"none"})
		

		vegetal = 0.20;
		alternativa = alternativa - vegetal
		cuenta = resultado - vegetal 
		resultado = cuenta
		valor3 = valor3 - vegetal
		aceituna = ""
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button12").css({"opacity":"1", "z-index":"2"})
        $("div.button12e").css({"opacity":"0", "z-index":"1"})
	})
	$("div.button13").click(function(){

		$("div.vegetalselec5").css({"background-image":"url(pictures/ceebolla.png)"})

		vegetal = 0.15;
		cuenta = resultado + vegetal
		resultado = cuenta
		valor3 = valor3 + vegetal
		cebolla = "Cebolla, "
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button13").css({"opacity":"0", "z-index":"1"})
        $("div.button13e").css({"opacity":"1", "z-index":"2"})
        
	})
	$("div.button13e").click(function(){

		$("div.vegetalselec5").css({"background-image":"none"})
		

		vegetal = 0.15;
		alternativa = alternativa - vegetal
		cuenta = resultado - vegetal 
		resultado = cuenta
		valor3 = valor3 - vegetal
		cebolla = ""
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button13").css({"opacity":"1", "z-index":"2"})
        $("div.button13e").css({"opacity":"0", "z-index":"1"})
	})
	$("div#avanzar2").click(function(){
		$("div#avanzar2").addClass('holi')
		$("div#avanzar2").css({"width":"0%"})
		$("div#avanzar2aux").css({"opacity":"0","z-index":"2"})
		$("section#aderesos").css({"margin-left":"2%","opacity":"1","z-index":"8"})
		$("div#regresar2").css({"width":"0%"})
		$("div#regresar2aux").css({"opacity":"0","z-index":"2"})
		$("div#titulo").html("Escoge los aderesos que desees!")
		$("div#tituloaux").html("Escoge los aderesos que desees!")
		function change66(){
		if($("div#avanzar2").hasClass('holi')){
			$("div#avanzar3").css({"width":"14%"})
			$("div#avanzar3aux").css({"opacity":"1","z-index":"3"})
			$("div#avanzar2").removeClass('holi')
			$("div#regresar3").css({"width":"14%"})
			$("div#regresar3aux").css({"opacity":"1","z-index":"3"})
		}
	}
	setInterval(change66, 2500)

	})
	$("div#avanzar2aux").click(function(){
		$("div#avanzar2").addClass('holi')
		$("div#avanzar2").css({"width":"0%"})
		$("div#avanzar2aux").css({"opacity":"0","z-index":"2"})
		$("section#aderesos").css({"margin-left":"2%","opacity":"1","z-index":"8"})
		$("div#regresar2").css({"width":"0%"})
		$("div#regresar2aux").css({"opacity":"0","z-index":"2"})
		$("div#titulo").html("Escoge los aderesos que desees!")
		$("div#tituloaux").html("Escoge los aderesos que desees!")
		function change666(){
		if($("div#avanzar2").hasClass('holi')){
			$("div#avanzar3").css({"width":"14%"})
			$("div#avanzar3aux").css({"opacity":"1","z-index":"3"})
			$("div#avanzar2").removeClass('holi')
			$("div#regresar3").css({"width":"14%"})
			$("div#regresar3aux").css({"opacity":"1","z-index":"3"})
		}
	}
	setInterval(change666, 2500)

	})
	$("div#regresar3").click(function(){
		$("section#aderesos").css({"margin-left":"58%","opacity":"0","z-index":"1"})
		$("div.aderezoselec1").css({"background-image":"none"})
		$("div.aderezoselec2").css({"background-image":"none"})
		$("div.aderezoselec3").css({"background-image":"none"})
		$("div.aderezoselec4").css({"background-image":"none"})
		$("div.vegetalselec1").css({"background-image":"none"})
		$("div.vegetalselec2").css({"background-image":"none"})
		$("div.vegetalselec3").css({"background-image":"none"})
		$("div.vegetalselec4").css({"background-image":"none"})
		$("div.vegetalselec5").css({"background-image":"none"})
		$("div.button14").css({"opacity":"1", "z-index":"2"})
        $("div.button14e").css({"opacity":"0", "z-index":"1"})
        $("div.button15").css({"opacity":"1", "z-index":"2"})
        $("div.button15e").css({"opacity":"0", "z-index":"1"})
        $("div.button16").css({"opacity":"1", "z-index":"2"})
        $("div.button16e").css({"opacity":"0", "z-index":"1"})
        $("div.button17").css({"opacity":"1", "z-index":"2"})
        $("div.button17e").css({"opacity":"0", "z-index":"1"})
        $("div.button9").css({"opacity":"1", "z-index":"2"})
        $("div.button9e").css({"opacity":"0", "z-index":"1"})
        $("div.button10").css({"opacity":"1", "z-index":"2"})
        $("div.button10e").css({"opacity":"0", "z-index":"1"})
        $("div.button11").css({"opacity":"1", "z-index":"2"})
        $("div.button11e").css({"opacity":"0", "z-index":"1"})
        $("div.button12").css({"opacity":"1", "z-index":"2"})
        $("div.button12e").css({"opacity":"0", "z-index":"1"})
        $("div.button13").css({"opacity":"1", "z-index":"2"})
        $("div.button13e").css({"opacity":"0", "z-index":"1"})
		$("div#avanzar2").css({"width":"14%"})
		$("div#avanzar2aux").css({"opacity":"1", "z-index":"3"})
		$("div#avanzar3").css({"width":"0%"})
		$("div#avanzar3aux").css({"opacity":"0", "z-index":"2"})
		$("div#regresar3").css({"width":"0%"})
		$("div#regresar3aux").css({"opacity":"0", "z-index":"2"})
		$("div#regresar2").css({"width":"14%"})
		$("div#regresar2aux").css({"opacity":"1", "z-index":"3"})
		cuenta = valor3
		$("div.cuenta").html("$ "+cuenta.toFixed(2))
		pepino = ""
		pimiento = ""
		cebolla = ""
		tomate = ""
		aceituna = ""
		ketchup = ""
		mayonesa = ""
		mostaza = ""
		chile = ""
	})
    $("div#regresar3aux").click(function(){
		$("section#aderesos").css({"margin-left":"58%","opacity":"0","z-index":"1"})
		$("div.aderezoselec1").css({"background-image":"none"})
		$("div.aderezoselec2").css({"background-image":"none"})
		$("div.aderezoselec3").css({"background-image":"none"})
		$("div.aderezoselec4").css({"background-image":"none"})
		$("div.vegetalselec1").css({"background-image":"none"})
		$("div.vegetalselec2").css({"background-image":"none"})
		$("div.vegetalselec3").css({"background-image":"none"})
		$("div.vegetalselec4").css({"background-image":"none"})
		$("div.vegetalselec5").css({"background-image":"none"})
		$("div.button14").css({"opacity":"1", "z-index":"2"})
        $("div.button14e").css({"opacity":"0", "z-index":"1"})
        $("div.button15").css({"opacity":"1", "z-index":"2"})
        $("div.button15e").css({"opacity":"0", "z-index":"1"})
        $("div.button16").css({"opacity":"1", "z-index":"2"})
        $("div.button16e").css({"opacity":"0", "z-index":"1"})
        $("div.button17").css({"opacity":"1", "z-index":"2"})
        $("div.button17e").css({"opacity":"0", "z-index":"1"})
        $("div.button9").css({"opacity":"1", "z-index":"2"})
        $("div.button9e").css({"opacity":"0", "z-index":"1"})
        $("div.button10").css({"opacity":"1", "z-index":"2"})
        $("div.button10e").css({"opacity":"0", "z-index":"1"})
        $("div.button11").css({"opacity":"1", "z-index":"2"})
        $("div.button11e").css({"opacity":"0", "z-index":"1"})
        $("div.button12").css({"opacity":"1", "z-index":"2"})
        $("div.button12e").css({"opacity":"0", "z-index":"1"})
        $("div.button13").css({"opacity":"1", "z-index":"2"})
        $("div.button13e").css({"opacity":"0", "z-index":"1"})
		$("div#avanzar2").css({"width":"14%"})
		$("div#avanzar2aux").css({"opacity":"1", "z-index":"3"})
		$("div#avanzar3").css({"width":"0%"})
		$("div#avanzar3aux").css({"opacity":"0", "z-index":"2"})
		$("div#regresar3").css({"width":"0%"})
		$("div#regresar3aux").css({"opacity":"0", "z-index":"2"})
		$("div#regresar2").css({"width":"14%"})
		$("div#regresar2aux").css({"opacity":"1", "z-index":"3"})
		cuenta = valor3
		$("div.cuenta").html("$ "+cuenta.toFixed(2))
		pepino = ""
		pimiento = ""
		cebolla = ""
		tomate = ""
		aceituna = ""
		ketchup = ""
		mayonesa = ""
		mostaza = ""
		chile = ""
	})
	$("div.button14").click(function(){

		$("div.aderezoselec1").css({"background-image":"url(pictures/1111.png)"})

		adereso = 0.05;
		cuenta = resultado + adereso
		resultado = cuenta
		ketchup = "Salsa de tomate, "
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button14").css({"opacity":"0", "z-index":"1"})
        $("div.button14e").css({"opacity":"1", "z-index":"2"})
        
	})
	$("div.button14e").click(function(){

		$("div.aderezoselec1").css({"background-image":"none"})
		

		adereso = 0.05;
		alternativa = alternativa - adereso
		cuenta = resultado - adereso 
		resultado = cuenta
		ketchup = ""
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button14").css({"opacity":"1", "z-index":"2"})
        $("div.button14e").css({"opacity":"0", "z-index":"1"})
	})
	$("div.button15").click(function(){

		$("div.aderezoselec2").css({"background-image":"url(pictures/2222.png)"})

		adereso = 0.05;
		cuenta = resultado + adereso
		resultado = cuenta
		mayonesa = "Mayonesa, "
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button15").css({"opacity":"0", "z-index":"1"})
        $("div.button15e").css({"opacity":"1", "z-index":"2"})
        
	})
	$("div.button15e").click(function(){

		$("div.aderezoselec2").css({"background-image":"none"})
		

		adereso = 0.05;
		alternativa = alternativa - adereso
		cuenta = resultado - adereso 
		resultado = cuenta
		mayonesa = ""
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button15").css({"opacity":"1", "z-index":"2"})
        $("div.button15e").css({"opacity":"0", "z-index":"1"})
	})
	$("div.button16").click(function(){

		$("div.aderezoselec3").css({"background-image":"url(pictures/3333.png)"})

		adereso = 0.05;
		cuenta = resultado +adereso
		resultado = cuenta
		mostaza = "Mostaza, "
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button16").css({"opacity":"0", "z-index":"1"})
        $("div.button16e").css({"opacity":"1", "z-index":"2"})
        
	})
	$("div.button16e").click(function(){

		$("div.aderezoselec3").css({"background-image":"none"})
		

		adereso = 0.05;
		alternativa = alternativa - adereso
		cuenta = resultado - adereso 
		resultado = cuenta
		mostaza = ""
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button16").css({"opacity":"1", "z-index":"2"})
        $("div.button16e").css({"opacity":"0", "z-index":"1"})
	})
	$("div.button17").click(function(){

		$("div.aderezoselec4").css({"background-image":"url(pictures/4444.png)"})

		adereso = 0.05;
		cuenta = resultado + adereso
		resultado = cuenta
		chile = "Chile, "
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
         $("div.button17").css({"opacity":"0", "z-index":"1"})
        $("div.button17e").css({"opacity":"1", "z-index":"2"})
        
	})
	$("div.button17e").click(function(){

		$("div.aderezoselec4").css({"background-image":"none"})
		

		adereso = 0.05;
		alternativa = alternativa - adereso
		cuenta = resultado - adereso 
		resultado = cuenta
		chile = ""
        $("div.cuenta").html("$ "+cuenta.toFixed(2))
        $("div.cuentaaux").html("$ "+cuenta.toFixed(2))
        $("div.button17").css({"opacity":"1", "z-index":"2"})
        $("div.button17e").css({"opacity":"0", "z-index":"1"})
	})
	$("div#avanzar3").click(function(){

		var descripcion = pan_oregano + pan_blanco + pan_light + pan_integral + jamon_suizo + jamon_serrano + jamon_york + pechuga_depollo +  pepino + cebolla + tomate + pimiento + ketchup + aceituna + mostaza + mayonesa + chile 
        window.location.href="facturacustom.php?precio="+cuenta+"&descripcion="+descripcion;

	})
})

