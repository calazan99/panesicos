$(function(){
	var control = 0;
	var index = 0;
	var $imagenes;
	var total = 0;

	$("#galeria a").on("click", function(){
		index = $(this).index();
		$("#colecciones").fadeIn();
		$("#colecciones .coleccion").hide();
		$("#colecciones .coleccion").eq(index).fadeIn();
		
		$("#colecciones .coleccion").eq(index).children().eq(0).children().hide();
		$("#colecciones .coleccion").eq(index).children().eq(0).children().eq(0).fadeIn();

		control=0;
		$imagenes = $("#" + $(this).attr("href"));
		total = $imagenes.children().size();
		return false;
	});

	$(".cerrar a").on("click", function(){
		$("#colecciones").fadeOut();
		return false;
	});

	$(".controles .siguiente").on("click", function(){
		control = (control + 1) % total;
		$imagenes.children().hide();
		$imagenes.children().eq(control).fadeIn();
		return false;
	});
	$(".controles .anterior").on("click", function(){
	control = (control - 1) % total;
	$imagenes.children().hide();
	$imagenes.children().eq(control).fadeIn();
	});
});