<?php
session_start();
require("../../lib/database.php");
require("../../lib/validator.php");
if (!isset($_SESSION['usuario_cliente'])) {
	echo "<script> alert('Debe iniciar sesion primero'); </script>";
	echo '<script> window.location="main/login.php"; </script>';
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="style/Pag2.css">
	<link rel="stylesheet" href="style/otros.css">
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src='js/Pag2.js'></script>
</head>
<header class="header2">
<div class="wrapper">
			<div class="logo"><img src="pictures/Logo2.png"></div>
			<nav>
				<a href="menu.php">Menú a la Carta</a>
				<a href="cocina.php">Crea Tu Pan</a>
				<a href="main/logout.php">Cerrar sesión</a>
			</nav>
		</div>
</header>
<!--Fijate adonde sale esto ok, no puedo ver porque el wamp no me finciona!-->
<p class="displayon" id="sesion"><a class="off" href="main/logout.php">Cerrar Sesion</a></p>

<body>
<p class="displayon" id="decision">Que prefieres comer hoy?</p>
<div id="galeria">
<p class="displayon"  id="gale">Uno de nuestros exquicitos panes a la carta</p>
<div class="lineaoff2" id="linea2"></div>
<div class="imgoff" id="img1">
</div>
<a href="menu.php"><div class="nulo" id="botongale"><center>Ordenar!</center></div></a>
</div>
<div id="o">
<p class="displayon" id="o">Ó</p>
</div>
<div id="crear">
<p class="displayon" id="crea">Crear tu propio pan</p>
<div class="lineaoff2" id="linea3"></div>
<div class="imgoff2" id="img2"></div>
<a href="cocina.php"><div class="nulo" id="botongale"><center>Crear!</center></div></a>
</div>
</body>
</html>
