<?php
include "validar_pagina.php";
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="style/pedido.css">
	<link rel="stylesheet" href="style/confirmacion.css">
</head>
<header class="header2">
<div class="wrapper">
			<div class="logo"><img src="pictures/Logo2.png"></div>
			<nav>
				<a href="">Inicio</a>
                <a href="todos.php">Registro de Pedidos</a>
                <a href="pedidos_admin.php">Pedidos del dia</a>
                <a href="ingredientes_admin.php">Administrar ingredientes</a>
				<a href="main/logout.php">Cerrar sesión</a>

			</nav>
		</div>
</header>
</html>
<?php
$id = $_GET['id'];

echo "
<p>¿Estas seguro de que quieres entregar el pedido?</p>
<div id='conf'>
<table>
<tr>
<th><a href='entregar.php?id=".$id."'> Si </a></th>
<th><a href='pedidos_admin.php'> No </a></th>
</tr>
</table>
</div>
";
?>