<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <!--Import Google Icon Font-->
    <link href="css/icons.css" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
     
    <?php
    include("inc/menu.php");
    ?>

     <body background="fondo2.jpg">

       <div>
    <h5 class="center-align">Sucursales</h5>
         </div>

    <div class="container">
        
        <table class="striped">
        <thead>
          <tr>
              <th data-field="id">Nombre</th>
              <th data-field="name"></th>
              <th data-field="price">Ubicacion</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>Sucursal Merlior</td>
            <td><img src= "img/sucursales/su1.jpg" width = "300" height = "200"></td>
            <td><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1938.0531308005745!2d-89.18904956953496!3d13.712013536874915!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f6330bd7db797e9%3A0x43d8139b00f8c20b!2sSan+Miguelito%2C+San+Salvador!5e0!3m2!1ses-419!2ssv!4v1487917614042" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe></td>
          </tr>
          <tr>
            <td>Sucursal San miguelito</td>
            <td><img src= "img/sucursales/su2.jpg" width = "300" height = "200"></td>
            <td><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1938.0531308005745!2d-89.18904956953496!3d13.712013536874915!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f6330bd7db797e9%3A0x43d8139b00f8c20b!2sSan+Miguelito%2C+San+Salvador!5e0!3m2!1ses-419!2ssv!4v1487917614042" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe></td>
          </tr>
          <tr>
            <td>Socursal Buenos aires</td>
            <td><img src= "img/sucursales/su3.jpg" width = "300" height = "200"></td>
            <td><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1938.0531308005745!2d-89.18904956953496!3d13.712013536874915!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f6330bd7db797e9%3A0x43d8139b00f8c20b!2sSan+Miguelito%2C+San+Salvador!5e0!3m2!1ses-419!2ssv!4v1487917614042" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe></td>
          </tr>
        </tbody>
      </table>
        
        </form>
    </div>




    <?php
    require("inc/pie.php")
    ?>
</body>
</html>
