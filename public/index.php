<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <!--Import Google Icon Font-->
    <link href="css/icons.css" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
     
    <?php
    include("inc/menu.php");
    ?>

     <body background="fondo2.jpg">

 <div class="slider">
      <ul class="slides">
        <li>
          <img src="img/slider/pan1.jpg" alt=""> <!-- random image -->
          <div class="caption rigth-align">
            <h3><b><FONT FACE="Castellar"  color="black"></FONT></b></h3>
            <h5 class="light grey-text text-lighten-3"><b><FONT FACE="Castellar"  color="black"></FONT></b></h5>
          </div>
        </li>
        <li>
          <img src="img/slider/pan2.jpg" alt=""> <!-- random image -->
          <div class="caption left-align">
            <h3><b><FONT FACE="Castellar"  color="black"></FONT></b></h3>
            <h5 class="light grey-text text-lighten-3"><b><FONT FACE="Castellar"  color="black"></FONT></b></h5>
          </div>
        </li>
        <li>
          <img src="img/slider/pan3.jpg" alt=""> <!-- random image -->
          <div class="caption right-align">
            <h3><b><FONT FACE="Castellar"  color="black"></FONT></b></h3>
            <h5 class="light grey-text text-lighten-3"><b><FONT FACE="Castellar"  color="black"></FONT></b></h5>
          </div>
        </li>
        <li>
          <img src="img/slider/pan4.jpg" alt=""> <!-- random image -->
          <div class="caption center-align">
            <h3><b><FONT FACE="Castellar"  color="black"></FONT></b></h3>
            <h5 class="light grey-text text-lighten-3"><b><FONT FACE="Castellar"  color="black"></FONT></b></h5>
          </div>
        </li>
      </ul>
    </div>
    

    <div class="row">
        <div class="card col s12 m4">
            <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" src="img/product/sub.jpg">
            </div>
            <div class="card-content">
                <span class="card-title activator grey-text text-darken-4">Comida de calidad<i class="material-icons right">more_vert</i></span>
                <p><a href="#"><i class="material-icons"></i></a></p>
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Todo lo que quieres<i class="material-icons right">close</i></span>
                <p>Todo tipo de panes, especialidades, frutas, verduras y otras cosas importantes que hacen que Panes Icos sea
                una pag web para satisfacer al cliente, dandole lo que pide.</p>
            </div>
        </div>

        <div class="card col s12 m4">
            <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" src="img/product/sodass.jpg">
            </div>
            <div class="card-content">
                <span class="card-title activator grey-text text-darken-4">Todas las bebidas que quieras!!<i class="material-icons right">more_vert</i></span>
                <p><a href="#"><i class="material-icons">t</i></a></p>
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Sin limites<i class="material-icons right">close</i></span>
                <p>Ofrecemos todo tipo de sodas que se tiene en la dispocicion, hay variedades de sabores y pueden ir junto 
                con la comida principal, para mas informacion puede visitar las ofertas para ver mas especificamente.</p>
            </div>
        </div>

        <div class="card col s12 m4">
            <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" src="img/product/galleta1.jpg">
            </div>
            <div class="card-content">
                <span class="card-title activator grey-text text-darken-4">Nuestro plus<i class="material-icons right">more_vert</i></span>
                <p><a href="#"><i class="material-icons"></i></a></p>
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Desert Time!<i class="material-icons right">close</i></span>
                <p>Se brinda tambien galletas que al finalizar de comer puede disfrutar de diferentes tipos de galletas
                que el lugar tiene para usted, mas informacion puede consular las ofertas.</p>
            </div>
        </div>
    </div>

<center>
<h3>Acerca de nosotros</h3>
</center>



  <div class="row valign-wrapper">
    <div class="col s6 offset-s3 valign">
      <div class="card blue-grey darken-1">
        <div class="card-content white-text">
          <span class="card-title">Nuestra Mision</span>
          <p>Proporcionar a empresarios de todo el mundo herramientas y conocimientos que permitan alcanzar el éxito en la industria
           de la restauración ofreciendo siempre un servicio excelente, con productos de calidad, preparado según sus criterios y delante del cliente.</p>
        </div>
        <div class="card-action">
        </div>
      </div>
    </div>
  </div>


  <div class="row valign-wrapper">
    <div class="col s6 offset-s3 valign">
      <div class="card blue-grey darken-1">
        <div class="card-content white-text">
          <span class="card-title">Nuestra Vision</span>
          <p>Deleitar a cada cliente de forma tal que quiera contarles a sus amigos sobre unos deliciosos sándwiches recién preparados, a su gusto y a un gran valor.
           ! Una experiencia excepcional!</p>
        </div>
        <div class="card-action">
        </div>
      </div>
    </div>
  </div>



  <div class="row valign-wrapper">
    <div class="col s6 offset-s3 valign">
      <div class="card blue-grey darken-1">
        <div class="card-content white-text">
          <span class="card-title">Nuestros Objetivos</span>
          <p>Atraer al cliente y conseguir que vuelva. Dicho de otro modo, la cadena quiere que el cliente potencial conozca el producto,
           lo pruebe y regrese convencido por un precio asequible y una amplia gama de productos.</p>
        </div>
        <div class="card-action">
        </div>
      </div>
    </div>
  </div>



  <div class="row valign-wrapper">
    <div class="col s6 offset-s3 valign">
      <div class="card blue-grey darken-1">
        <div class="card-content white-text">
          <span class="card-title">Nuestros Valores</span>
          <p> 
    · Servicio
    · Honestidad
    · Lealtad
    · Compromiso
    · Respeto
    · Responsabilidad
    
    </p>
        </div>
        <div class="card-action">
        </div>
      </div>
    </div>
  </div>
</div>
            
     


    <?php
    require("inc/pie.php")
    ?>
</body>
</html>




