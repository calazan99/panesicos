<?php
// mandar a llamaar a las librerias
require("../lib/page.php");
Page::header("clientes");
//consulta de buscar por letra
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM clientes WHERE nombre_cliente LIKE ? ORDER BY nombre_cliente";
	$params = array("%$search%");
}
else
{
	//seleccionar la tabla clientes
	$sql = "SELECT * FROM clientes  ORDER BY nombre_cliente";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!--mandar a llamr la consulta de forma grafica-->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn waves-effect green'><i class='material-icons'>check_circle</i></button> 	
		</div>
	</div>
</form>
<table class='striped'>
	<thead>
		<tr>
			<th>NOMBRE</th>
			<th>APELLIDO</th>
			<th>DIRECCION</th>
			<th>TELEFONO</th>
			<th>ESTADO</th>
			<th>USUARIO</th>
			<th>CONTRASEÑA</th>
		</tr>
	</thead>
	<tbody>

<?php
	foreach($data as $row)
	{
		//se manda a llamar por filas
		print("
			<tr>
				<td>".$row['nombre_cliente']."</td>
				<td>".$row['apellido_cliente']."</td>
				<td>".$row['direccion_cliente']."</td>
				<td>".$row['telefono_cliente']."</td>
                
				<td>
		");
		if($row['estado_cliente'] == 1)
		{
			print("<i class='material-icons'>visibility</i>");
		}
		else
		{
			print("<i class='material-icons'>visibility_off</i>");
		}
		print("
				</td>
				<td>".$row['usuario_cliente']."</td>
				<td>".$row['contrasenia_cliente']."</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "../main/index.php");
}
Page::footer();
?>