<?php
// mandar a llamaar a las librerias
ob_start();
require("../lib/page.php");
Page::header("combos");

if(!empty($_POST))
{
	//busca por letra
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM combos WHERE nombre_combo LIKE ? ORDER BY nombre_combo";
	$params = array("%$search%");
}
else
{
	//selecciona la consulta
	$sql = "SELECT * FROM combos  ORDER BY nombre_combo";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!--Forma grafica de buscar-->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn waves-effect green'><i class='material-icons'>check_circle</i></button> 	
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<table class='striped'>
	<thead>
		<tr>
			<th>IMAGEN</th>
			<th>NOMBRE</th>
			<th>PRECIO ($)</th>
			<th>ESTADO</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>

<?php
	foreach($data as $row)
	{
		// se agregue a la fila los nombres de la tabla
		print("
			<tr>
				<td><img src='data:image/*;base64,".$row['imagen_combo']."' class='materialboxed' width='100' height='100'></td>
				<td>".$row['nombre_combo']."</td>
				<td>".$row['precio_combo']."</td>
				<td>
		");
		if($row['estado_combo'] == 1)
		{
			print("<i class='material-icons'>visibility</i>");
		}
		else
		{
			print("<i class='material-icons'>visibility_off</i>");
		}
// botones para salvar y eliminar
		print("
				</td>
				<td>
					<a href='save.php?id=".$row['id_combo']."' class='blue-text'><i class='material-icons'>mode_edit</i></a>
					<a href='delete.php?id=".$row['id_combo']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
Page::footer();
?>