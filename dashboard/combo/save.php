<?php
// mandar a llamaar a las librerias
ob_start();
require("../lib/page.php");
if(empty($_GET['id'])) 
{
    // se verifica que los campos esten vacios
    Page::header("Agregar combos");
    $id = null;
    $nombre = null;
    $descripcion = null;
    $precio = null;
    $imagen = null;
    $estado = 1;
    
}
else
{
    //pselecciona el Id a modificar
    Page::header("Modificar combos");
    $id = $_GET['id'];
    $sql = "SELECT * FROM combos WHERE id_combo = ?";
    $params = array($id);
    $data = Database::getRow($sql, $params);
    $nombre = $data['nombre_combo'];
    $descripcion = $data['descripcion_combo'];
    $precio = $data['precio_combo'];
    $imagen = $data['imagen_combo'];
    $estado = $data['estado_combo'];
   
}

if(!empty($_POST))
{
    // valida el campo
    $_POST = Validator::validateForm($_POST);
  	$nombre = $_POST['nombre'];
  	$descripcion = $_POST['descripcion'];
    $precio = $_POST['precio'];
    $archivo = $_FILES['imagen'];
    $estado = $_POST['estado'];
   

    try 
    {
        if($nombre != "")
        {
            if($precio != "")
            {
                if($precio > 0)
                {
                    if($descripcion != "")
                    {
                         if($archivo['name'] != null)
                            {
                                $base64 = Validator::validateImage($archivo);
                                if($base64 != false)
                                {
                                    $imagen = $base64;
                                }
                                else
                                {
                                    throw new Exception("Ocurrió un problema con la imagen");
                                }
                            }
                            else
                            {
                                if($imagen == null)
                                {
                                    throw new Exception("Debe seleccionar una imagen");
                                }
                            }
                            if($id == null)
                            {
                                // inserta un registro
                                $sql = "INSERT INTO combos(nombre_combo, precio_combo, descripcion_combo,  imagen_combo, estado_combo) VALUES(?, ?, ?, ?, ?)";
                                $params = array($nombre, $precio, $descripcion, $imagen, $estado);
                            }
                            else
                            {
                                // actualiza un registro
                                $sql = "UPDATE combos SET nombre_combo = ?, descripcion_combo = ?, precio_combo = ?, imagen_combo = ?, estado_combo = ? WHERE id_combo = ?";
                                $params = array($nombre, $descripcion, $precio, $imagen, $estado,  $id);
                            }
                            Database::executeRow($sql, $params);
                            header("location: index.php");
                        }
                        
                    else
                    {
                        throw new Exception("Debe digitar una descripción");
                    }
                }
                else
                {
                    throw new Exception("El precio debe ser mayor que 0.00");
                }
            }
            else
            {
                throw new Exception("Debe ingresar el precio");
            }
        }
        else
        {
            throw new Exception("Debe digitar el nombre");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<!--Agrega a los campos las filas modificadas o agregadas-->
<form method='post' enctype='multipart/form-data'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>note_add</i>
          	<input id='nombre' type='text' name='nombre' class='validate' value='<?php print($nombre); ?>' required/>
          	<label for='nombre'>Nombre</label>
        </div>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>shopping_cart</i>
          	<input id='precio' type='number' name='precio' class='validate' max='999.99' min='0.01' step='any' value='<?php print($precio); ?>' required/>
          	<label for='precio'>Precio ($)</label>
        </div>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>description</i>
          	<input id='descripcion' type='text' name='descripcion' class='validate' value='<?php print($descripcion); ?>'/>
          	<label for='descripcion'>Descripción</label>
        </div>
      	<div class='file-field input-field col s12 m6'>
            <div class='btn waves-effect'>
                <span><i class='material-icons'>image</i></span>
                <input type='file' name='imagen' <?php print(($imagen == null)?"required":""); ?>/>
            </div>
            <div class='file-path-wrapper'>
                <input class='file-path validate' type='text' placeholder='Seleccione una imagen'/>
            </div>
        </div>
        <div class='input-field col s12 m6'>
            <span>Estado:</span>
            <input id='activo' type='radio' name='estado' class='with-gap' value='1' <?php print(($estado == 1)?"checked":""); ?>/>
            <label for='activo'><i class='material-icons left'>visibility</i></label>
            <input id='inactivo' type='radio' name='estado' class='with-gap' value='0' <?php print(($estado == 0)?"checked":""); ?>/>
            <label for='inactivo'><i class='material-icons left'>visibility_off</i></label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>