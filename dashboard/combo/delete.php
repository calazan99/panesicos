<?php
// mandar a llamaar a las librerias
ob_start();
require("../lib/page.php");
Page::header("Eliminar combo");

if(!empty($_GET['id'])) 
{
    $id = $_GET['id'];
}
else
{
	// al terminar la consulta se redirige al index
    header("location: index.php");
}
// se hace la consulta para eliminar
if(!empty($_POST))
{
	$id = $_POST['id'];
	try 
	{
		$sql = "DELETE FROM combos WHERE id_combo = ?";
	    $params = array($id);
	    Database::executeRow($sql, $params);
	    header("location: index.php");
	}
	catch (Exception $error) 
	{
		// al terminar la consulta se dirige al index
		Page::showMessage(2, $error->getMessage(), "index.php");
	}
}
?>
<!--Forma grafica de hacer la consulta-->
<form method='post'>
	<div class='row center-align'>
		<input type='hidden' name='id' value='<?php print($id); ?>'/>
		<button type='submit' class='btn waves-effect red'><i class='material-icons'>remove_circle</i></button>
		<a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
	</div>
</form>

<?php
Page::footer();
?>