<?php
// mandar a llamaar a las librerias
ob_start();
require("../lib/page.php");
if(empty($_GET['id'])) 
{
    //guarda los datos
    Page::header("Agregar tipos de quesos");
    $id = null;
    $nombre = null;
    $descripcion = null;
    $imagen = null;
    $estado = 1;
    
}
else
{
    //modifica los datos
    Page::header("Modificar tipos de quesos");
    $id = $_GET['id'];
    $sql = "SELECT * FROM tipos_quesos WHERE id_tipo_queso = ?";
    $params = array($id);
    $data = Database::getRow($sql, $params);
    $nombre = $data['queso'];
    $descripcion = $data['descripcion_queso'];
    $imagen = $data['imagen_queso'];
    $estado = $data['estado_queso'];
   
}

if(!empty($_POST))
{
    //valida los valores
    $_POST = Validator::validateForm($_POST);
  	$nombre = $_POST['nombre'];
  	$descripcion = $_POST['descripcion'];
    $archivo = $_FILES['imagen'];
    $estado = $_POST['estado'];
   

    try 
    {
        if($nombre != "")
        {
               if($descripcion != "")
                    {
                         if($archivo['name'] != null)
                            {
                                $base64 = Validator::validateImage($archivo);
                                if($base64 != false)
                                {
                                    $imagen = $base64;
                                }
                                else
                                {
                                    throw new Exception("Ocurrió un problema con la imagen");
                                }
                            }
                            else
                            {
                                if($imagen == null)
                                {
                                    throw new Exception("Debe seleccionar una imagen");
                                }
                            }
                            if($id == null)
                            {
                                $sql = "INSERT INTO tipos_quesos(queso,  descripcion_queso,  imagen_queso, estado_queso) VALUES(?, ?, ?, ?)";
                                $params = array($nombre, $descripcion, $imagen, $estado);
                            }
                            else
                            {
                                $sql = "UPDATE tipos_quesos SET queso = ?, descripcion_queso = ?,  imagen_queso = ?, estado_queso = ? WHERE id_tipo_queso = ?";
                                $params = array($nombre, $descripcion, $imagen, $estado,  $id);
                            }
                            Database::executeRow($sql, $params);
                            header("location: index.php");
                        }
                        
                    else
                    {
                        throw new Exception("Debe digitar una descripción");
                    }
                }
        else
        {
            throw new Exception("Debe digitar el nombre");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<!--Guarda los datos de manera grafica-->
<form method='post' enctype='multipart/form-data'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>note_add</i>
          	<input id='nombre' type='text' name='nombre' class='validate' value='<?php print($nombre); ?>' required/>
          	<label for='nombre'>Nombre</label>
        </div>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>description</i>
          	<input id='descripcion' type='text' name='descripcion' class='validate' value='<?php print($descripcion); ?>'/>
          	<label for='descripcion'>Descripción</label>
        </div>
      	<div class='file-field input-field col s12 m6'>
            <div class='btn waves-effect'>
                <span><i class='material-icons'>image</i></span>
                <input type='file' name='imagen' <?php print(($imagen == null)?"required":""); ?>/>
            </div>
            <div class='file-path-wrapper'>
                <input class='file-path validate' type='text' placeholder='Seleccione una imagen'/>
            </div>
        </div>
        <div class='input-field col s12 m6'>
            <span>Estado:</span>
            <input id='activo' type='radio' name='estado' class='with-gap' value='1' <?php print(($estado == 1)?"checked":""); ?>/>
            <label for='activo'><i class='material-icons left'>visibility</i></label>
            <input id='inactivo' type='radio' name='estado' class='with-gap' value='0' <?php print(($estado == 0)?"checked":""); ?>/>
            <label for='inactivo'><i class='material-icons left'>visibility_off</i></label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>