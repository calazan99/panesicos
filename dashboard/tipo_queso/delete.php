<?php
// mandar a llamaar a las librerias
ob_start();
require("../lib/page.php");
Page::header("Eliminar tipo de queso");

if(!empty($_GET['id'])) 
{
    $id = $_GET['id'];
}
else
{
	//redirige al index
    header("location: index.php");
}

if(!empty($_POST))
{
	$id = $_POST['id'];
	try 
	{
		//elimina los datos de la tabla
		$sql = "DELETE FROM tipos_quesos WHERE id_tipo_queso = ?";
	    $params = array($id);
	    Database::executeRow($sql, $params);
	    header("location: index.php");
	}
	catch (Exception $error) 
	{
		Page::showMessage(2, $error->getMessage(), "index.php");
	}
}
?>
<!--Elimina de modo grafico-->
<form method='post'>
	<div class='row center-align'>
		<input type='hidden' name='id' value='<?php print($id); ?>'/>
		<button type='submit' class='btn waves-effect red'><i class='material-icons'>remove_circle</i></button>
		<a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
	</div>
</form>

<?php
Page::footer();
?>