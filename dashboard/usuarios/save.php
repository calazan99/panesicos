<?php
// mandar a llamaar a las librerias
ob_start();
require("../lib/page.php");
if(empty($_GET['id'])) 
{
    //Agrega una nueva consulta
    Page::header("Agregar usuario");
    $id = null;
    $nombre_usuario = null;
    $apellido_usuario = null;
    $correo_usuario = null;
    $usuario = null;
}
else
{
    //modifica una consulta
    Page::header("Modificar usuario");
    $id = $_GET['id'];
    $sql = "SELECT * FROM usuarios WHERE id_usuario = ?";
    $params = array($id);
    $data = Database::getRow($sql, $params);
    $nombre_usuario = $data['nombre_usuario'];
    $apellido_usuario = $data['apellido_usuario'];
    $correo_usuario = $data['correo_usuario'];
    $usuario = $data['usuario'];
}

if(!empty($_POST))
{
    // valida los datos
    $_POST = Validator::validateForm($_POST);
  	$nombre_usuario = $_POST['nombre_usuario'];
  	$apellido_usuario = $_POST['apellido_usuario'];
    $correo_usuario = $_POST['correo_usuario'];

    try 
    {
      	if($nombre_usuario != "" && $apellido_usuario != "")
        {
            if($correo_usuario != "")
            {
                if($id == null)
                {
                    $usuario = $_POST['usuario'];
                    if($usuario != "")
                    {
                        $contrasenia1 = $_POST['contrasenia1'];
                        $contrasenia2 = $_POST['contrasenia2'];
                        if($contrasenia1 != "" && $contrasenia2 != "")
                        {
                            if($contrasenia1 == $contrasenia2)
                            {
                                $contrasenia = password_hash($contrasenia1, PASSWORD_DEFAULT);
                                $sql = "INSERT INTO usuarios(nombre_usuario, apellido_usuario, correo_usuario, usuario, contrasenia) VALUES(?, ?, ?, ?, ?)";
                                $params = array($nombre_usuario, $apellido_usuario, $correo_usuario, $usuario, $contrasenia);
                            }
                            else
                            {
                                throw new Exception("Las contraseñas no coinciden");
                            }
                        }
                        else
                        {
                            throw new Exception("Debe ingresar ambas contraseñas");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe ingresar un usuario");
                    }
                }
                else
                {
                    $sql = "UPDATE usuarios SET nombre_usuario = ?, apellido_usuario = ?, correo_usuario = ? WHERE id_usuario = ?";
                    $params = array($nombre_usuario, $apellido_usuario, $correo_usuario, $id);
                }
                Database::executeRow($sql, $params);
                header("location: index.php");
            }
            else
            {
                throw new Exception("Debe ingresar un correo_usuario electrónico");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<!--Guarda datos de manera grafica-->
<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombre_usuario' type='text' name='nombre_usuario' class='validate' value='<?php print($nombre_usuario); ?>' required/>
          	<label for='nombre_usuario'>nombre_usuario</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellido_usuario' type='text' name='apellido_usuario' class='validate' value='<?php print($apellido_usuario); ?>' required/>
            <label for='apellido_usuario'>apellido_usuario</label>
        </div>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input id='correo_usuario' type='email' name='correo_usuario' class='validate' value='<?php print($correo_usuario); ?>' required/>
            <label for='correo_usuario'>correo_usuario</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='usuario' type='text' name='usuario' class='validate' <?php print("value='$usuario' "); print(($id == null)?"required":"disabled"); ?>/>
            <label for='usuario'>usuario</label>
        </div>
    </div>
    <?php
    if($id == null)
    {
    ?>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='contrasenia1' type='password' name='contrasenia1' class='validate' required/>
            <label for='contrasenia1'>Contraseña</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='contrasenia2' type='password' name='contrasenia2' class='validate' required/>
            <label for='contrasenia2'>Confirmar contraseña</label>
        </div>
    </div>
    <?php
    }
    ?>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>