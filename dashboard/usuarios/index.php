<?php
// mandar a llamaar a las librerias
ob_start();
require("../lib/page.php");
Page::header("usuarios");

if(!empty($_POST))
{
	//busca los datos por letra
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM usuarios WHERE apellido_usuario LIKE ? OR nombre_usuario LIKE ? ORDER BY apellido_usuario";
	$params = array("%$search%", "%$search%");
}
else
{
	$sql = "SELECT * FROM usuarios ORDER BY apellido_usuario";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!--Busca los datos de manera grafica-->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn waves-effect green'><i class='material-icons'>check_circle</i></button> 	
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<table class='striped'>
	<thead>
		<tr>
			<th>apellido_usuario</th>
			<th>nombre_usuario</th>
			<th>correo_usuario</th>
			<th>usuario</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>

<?php
	foreach($data as $row)
	{
		// enseña los usuarios de las tablas
		print("
			<tr>
				<td>".$row['apellido_usuario']."</td>
				<td>".$row['nombre_usuario']."</td>
				<td>".$row['correo_usuario']."</td>
				<td>".$row['usuario']."</td>
				<td>
					<a href='save.php?id=".$row['id_usuario']."' class='blue-text'><i class='material-icons'>edit</i></a>
					<a href='delete.php?id=".$row['id_usuario']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
Page::footer();
?>