<?php
// mandar a llamaar a las librerias
require("../lib/page.php");
Page::header("Registrar primer usuario");

$sql = "SELECT * FROM usuarios";
$data = Database::getRows($sql, null);
if($data != null)
{
    header("location: login.php");
}

if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$nombre_usuario = $_POST['nombre_usuario'];
  	$apellido_usuario = $_POST['apellido_usuario'];
    $correo_usuario = $_POST['correo_usuario'];
    $usuario = $_POST['usuario'];
    $contrasenia1 = $_POST['contrasenia1'];
    $contrasenia2 = $_POST['contrasenia2'];

    try 
    {
      	if($nombre_usuario != "" && $apellido_usuario != "")
        {
            if($correo_usuario != "")
            {
                if($usuario != "")
                {
                    if($contrasenia1 != "" && $contrasenia2 != "")
                    {
                        if($contrasenia1 == $contrasenia2)
                        {
                            $contrasenia = password_hash($contrasenia1, PASSWORD_DEFAULT);
                            $sql = "INSERT INTO usuarios(nombre_usuario, apellido_usuario, correo_usuario, usuario, contrasenia) VALUES(?, ?, ?, ?, ?)";
                            $params = array($nombre_usuario, $apellido_usuario, $correo_usuario, $usuario, $contrasenia);
                            Database::executeRow($sql, $params);
                            Page::showMessage(1, "Operación satisfactoria", "login.php");
                        }
                        else
                        {
                            throw new Exception("Las contraseñas no coinciden");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe ingresar ambas contraseñas");
                    }
                }
                else
                {
                    throw new Exception("Debe ingresar un usuario");
                }
            }
            else
            {
                throw new Exception("Debe ingresar un correo_usuario electrónico");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
else
{
    $nombre_usuario = null;
    $apellido_usuario = null;
    $correo_usuario = null;
    $usuario = null;
}
?>

<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombre_usuario' type='text' name='nombre_usuario' class='validate' value='<?php print($nombre_usuario); ?>' required/>
          	<label for='nombre_usuario'>nombre_usuario</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellido_usuario' type='text' name='apellido_usuario' class='validate' value='<?php print($apellido_usuario); ?>' required/>
            <label for='apellido_usuario'>apellido_usuario</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input id='correo_usuario' type='email' name='correo_usuario' class='validate' value='<?php print($correo_usuario); ?>' required/>
            <label for='correo_usuario'>correo_usuario</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='usuario' type='text' name='usuario' class='validate' value='<?php print($usuario); ?>' required/>
            <label for='usuario'>usuario</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='contrasenia1' type='password' name='contrasenia1' class='validate' required/>
            <label for='contrasenia1'>Contraseña</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='contrasenia2' type='password' name='contrasenia2' class='validate' required/>
            <label for='contrasenia2'>Confirmar contraseña</label>
        </div>
    </div>
    <div class='row center-align'>
 	    <button type='submit' class='btn waves-effect'><i class='material-icons'>send</i></button>
    </div>
</form>

<?php
Page::footer();
?>