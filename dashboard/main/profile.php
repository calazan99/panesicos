<?php
// mandar a llamaar a las librerias
require("../lib/page.php");
Page::header("Editar perfil");

if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$nombre_usuario = $_POST['nombre_usuario'];
  	$apellido_usuario = $_POST['apellido_usuario'];
    $correo_usuario = $_POST['correo_usuario'];
    $usuario = $_POST['usuario'];
    $contrasenia1 = $_POST['contrasenia1'];
    $contrasenia2 = $_POST['contrasenia2'];

    try 
    {
      	if($nombre_usuario != "" && $apellido_usuario != "")
        {
            if($correo_usuario != "")
            {
                if($usuario != "")
                {
                    if($contrasenia1 != "" || $contrasenia2 != "")
                    {
                        if($contrasenia1 == $contrasenia2)
                        {
                            $contrasenia = password_hash($contrasenia1, PASSWORD_DEFAULT);
                            $sql = "UPDATE usuarios SET nombre_usuario = ?, apellido_usuario = ?, correo_usuario = ?, usuario = ?, contrasenia = ? WHERE id_usuario = ?";
                            $params = array($nombre_usuario, $apellido_usuario, $correo_usuario, $usuario, $contrasenia, $_SESSION['id_usuario']);
                        }
                        else
                        {
                            throw new Exception("Las contraseñas no coinciden");
                        }
                    }
                    else
                    {
                        $sql = "UPDATE usuarios SET nombre_usuario = ?, apellido_usuario = ?, correo_usuario = ?, usuario = ? WHERE id_usuario = ?";
                        $params = array($nombre_usuario, $apellido_usuario, $correo_usuario, $usuario, $_SESSION['id_usuario']);
                    }
                    Database::executeRow($sql, $params);
                    Page::showMessage(1, "Operación satisfactoria", "index.php");
                }
                else
                {
                    throw new Exception("Debe ingresar un usuario");
                }
            }
            else
            {
                throw new Exception("Debe ingresar un correo_usuario electrónico");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
else
{
    $sql = "SELECT * FROM usuarios WHERE id_usuario = ?";
    $params = array($_SESSION['id_usuario']);
    $data = Database::getRow($sql, $params);
    $nombre_usuario = $data['nombre_usuario'];
    $apellido_usuario = $data['apellido_usuario'];
    $correo_usuario = $data['correo_usuario'];
    $usuario = $data['usuario'];
}
?>

<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombre_usuario' type='text' name='nombre_usuario' class='validate' value='<?php print($nombre_usuario); ?>' required/>
          	<label for='nombre_usuario'>nombre_usuario</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellido_usuario' type='text' name='apellido_usuario' class='validate' value='<?php print($apellido_usuario); ?>' required/>
            <label for='apellido_usuario'>apellido_usuario</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input id='correo_usuario' type='email' name='correo_usuario' class='validate' value='<?php print($correo_usuario); ?>' required/>
            <label for='correo_usuario'>correo_usuario</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='usuario' type='text' name='usuario' class='validate' value='<?php print($usuario); ?>' required/>
            <label for='usuario'>usuario</label>
        </div>
    </div>
    <div class='row center-align'>
        <label>CAMBIAR contrasenia</label>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='contrasenia1' type='password' name='contrasenia1' class='validate'/>
            <label for='contrasenia1'>Contraseña nueva</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='contrasenia2' type='password' name='contrasenia2' class='validate'/>
            <label for='contrasenia2'>Confirmar contraseña</label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='../main/index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>