<?php
// mandar a llamaar a las librerias
require("../lib/page.php");
Page::header("Iniciar sesión");

$sql = "SELECT * FROM usuarios";
$data = Database::getRows($sql, null);  
if($data == null)
{
    header("location: register.php");
}

if(!empty($_POST))
{
	$_POST = validator::validateForm($_POST);
  	$usuario = $_POST['usuario'];
  	$contrasenia = $_POST['contrasenia'];
  	try
    {
      	if($usuario != "" && $contrasenia != "")
  		{
  			$sql = "SELECT * FROM usuarios WHERE usuario = ?";
		    $params = array($usuario);
		    $data = Database::getRow($sql, $params);
		    if($data != null)
		    {
		    	$hash = $data['contrasenia'];
		    	if(password_verify($contrasenia, $hash)) 
		    	{
			    	$_SESSION['id_usuario'] = $data['id_usuario'];
			      	$_SESSION['usuario'] = $data['nombre_usuario']." ".$data['apellido_usuario'];
			      	header("location: index.php");
				}
				else 
				{
					throw new Exception("La contrasenia ingresada es incorrecta");
				}
		    }
		    else
		    {
		    	throw new Exception("El usuario ingresado no existe");
		    }
	  	}
	  	else
	  	{
	    	throw new Exception("Debe ingresar un usuario y una contrasenia");
	  	}
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>

<form method='post'>
	<div class='row'>
		<div class='input-field col s12 m6 offset-m3'>
			<i class='material-icons prefix'>person_pin</i>
			<input id='usuario' type='text' name='usuario' class='validate' required/>
	    	<label for='usuario'>Usuario</label>
		</div>
		<div class='input-field col s12 m6 offset-m3'>
			<i class='material-icons prefix'>security</i>
			<input id='contrasenia' type='password' name='contrasenia' class="validate" required/>
			<label for='contrasenia'>Contraseña</label>
		</div>
	</div>
	<div class='row center-align'>
		<button type='submit' class='btn waves-effect'><i class='material-icons'>send</i></button>
	</div>
</form>

<?php
Page::footer();
?>