<?php
// mandar a llamaar a las librerias
ob_start();
require("../lib/page.php");
require("../../lib/Zebra_Pagination.php");
Page::header("ingredientes principales");

if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM ingredientes_principales WHERE nombre_principal LIKE ? ORDER BY nombre_principal";
	$params = array("%$search%");
}
else
{
	$sql = "SELECT * FROM ingredientes_principales  ORDER BY nombre_principal";
	$params = null;
}
$data = Database::getRows($sql, $params);

$num_registro = Database::getCount($sql, $params);
$resul_x_pagina = 10;

$paginacion = new Zebra_Pagination();
$paginacion->records($num_registro);
$paginacion->records_per_page($resul_x_pagina);

$consulta = $sql.' LIMIT '.(($paginacion->get_page()-1)* $resul_x_pagina).','.$resul_x_pagina;

$data = Database::getRows($consulta, $params);


	
if($data != null)
{
?>
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn waves-effect green'><i class='material-icons'>check_circle</i></button> 	
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<table class='striped'>
	<thead>
		<tr>
			<th>IMAGEN</th>
			<th>NOMBRE</th>
			<th>PRECIO ($)</th>
			<th>ESTADO</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>

<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td><img src='data:image/*;base64,".$row['imagen_principal']."' class='materialboxed' width='100' height='100'></td>
				<td>".$row['nombre_principal']."</td>
				<td>".$row['precio_principal']."</td>
				<td>
		");
		if($row['estado_principal'] == 1)
		{
			print("<i class='material-icons'>visibility</i>");
		}
		else
		{
			print("<i class='material-icons'>visibility_off</i>");
		}
		print("
				</td>
				<td>
					<a href='save.php?id=".$row['id_ingrediente_principal']."' class='blue-text'><i class='material-icons'>mode_edit</i></a>
					<a href='delete.php?id=".$row['id_ingrediente_principal']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
	$paginacion->render();
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
Page::footer();
?>